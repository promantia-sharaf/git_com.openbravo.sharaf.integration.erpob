/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.utility;

public class ERPOBUtils {

  // EDL Processes
  public static final String INVOICE_EDL_PROCESS_ID = "A6EFEA94415340C792B450FC43519704";
  public static final String PROMOTION_EDL_PROCESS_ID = "D7337CA83B6C40CC97B09CA9DFB4210D";
  public static final String BRAND_EDL_PROCESS_ID = "2A6C6EA2FE694E95BE3FB2B17B5C2699";
  public static final String BUSINESS_PARTNER_EDL_PROCESS_ID = "035D9EA348174656B83E4CADE2380C46";
  public static final String PRODUCT_EDL_PROCESS_ID = "448DF336D1064BDB87DB71EA0137CF10";
  public static final String PRODUCT_ASSORTMENT_EDL_PROCESS_ID = "1BD3415307C843E18CD0A3B55E47521F";
  public static final String PRODUCT_BARCODE_EDL_PROCESS_ID = "5713DD98BCDD4A22B51A8D2224007532";
  public static final String PRODUCT_CATEGORY_EDL_PROCESS_ID = "9047A0E9FD9143F793C6913C71326963";
  public static final String PRODUCT_PRICE_EDL_PROCESS_ID = "2D00529CE2844E4D8E48A3F67035D5E9";
  public static final String PRODUCT_STOCK_EDL_PROCESS_ID = "A12A9B4DC88642538463A31E855B3CD3";
  public static final String PRODUCT_TAXCATEGORY_EDL_PROCESS_ID = "02D5412D9C1645BF90C13EB7CE775488";

  // Promotion Types

  // EDL Status
  public static final String EDLLine_Status_Error = "Error";

  public static boolean isSharafImportEDLProcess(String edlProcessId) {
    if (edlProcessId.equals(PROMOTION_EDL_PROCESS_ID) || edlProcessId.equals(BRAND_EDL_PROCESS_ID)
        || edlProcessId.equals(BUSINESS_PARTNER_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_ASSORTMENT_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_BARCODE_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_CATEGORY_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_PRICE_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_STOCK_EDL_PROCESS_ID)
        || edlProcessId.equals(PRODUCT_TAXCATEGORY_EDL_PROCESS_ID)) {
      return true;
    }
    return false;
  }

}