/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.utility;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.client.application.Note;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.alert.Alert;
import org.openbravo.model.ad.alert.AlertRecipient;
import org.openbravo.model.ad.alert.AlertRule;
import org.openbravo.model.ad.alert.AlertRuleTrl;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.utils.FormatUtilities;

public class AlertUtils {

  private static final String ALERT_STATUS_SOLVED = "SOLVED";

  public static final String BRAND_ALERT_RULE_ID = "99457DABE0504990A2F3107A3379EE56";
  public static final String PRODUCT_CATEGORY_ALERT_RULE_ID = "C00A7832DE8248A7B2F70D0667A4AD65";
  public static final String PRODUCT_ALERT_RULE_ID = "62948B18DE664306B536A69746BB5B3D";
  public static final String PRODUCT_BARCODE_ALERT_RULE_ID = "BE70DD47707C4259BA8AFD44479B78C8";
  public static final String PRODUCT_PRICE_ALERT_RULE_ID = "C351A72310C14BDF833832FA30CF5D90";
  public static final String PRODUCT_STOCK_ALERT_RULE_ID = "8B3E955DEBDC486DBBA97F0209A8FB55";
  public static final String PRODUCT_ASSORTMENT_ALERT_RULE_ID = "09E2C5A86A1648B0B4F8F4F1C6FBA5D5";
  public static final String BUSINESS_PARTNER_ALERT_RULE_ID = "A3B2E6807039433D88E25B19BDC8962A";
  public static final String SALES_POSTING_EXCEPTIONS_ALERT_RULE_ID = "B87A39AAB7A54D99BAB0A0412F242EE0";
  public static final String PROMOTION_ALERT_RULE_ID = "A40F421B86AC4D669178E73C74751F13";
  public static final String PRODUCT_TAXCATEGORY_ALERT_RULE_ID = "922B01D2859D4746B087CA722C4BD3E9";

  public static Alert createSendAlert(Client client, Organization organization, String alertRuleId,
      String description, String recordId, String referenceKeyId) {
    return createSendAlert(client, organization, alertRuleId, description, recordId,
        referenceKeyId, null, null, null);
  }

  public static Alert createSendAlert(Client client, Organization organization, String alertRuleId,
      String description, String recordId, String referenceKeyId, String description2,
      String tabId2, String recordId2) {
    Alert alert = null;
    if (StringUtils.isNotEmpty(alertRuleId)) {
      AlertRule alertRule = OBDal.getInstance().get(AlertRule.class, alertRuleId);
      if (alertRule != null && StringUtils.isNotEmpty(description)
          && StringUtils.isNotEmpty(recordId) && StringUtils.isNotEmpty(referenceKeyId)) {
        // Create note
        if (alertRule.getTab() != null) {
          createNote(alertRule.getTab(), referenceKeyId, description);
        }
        for (AlertRecipient alertRecipient : alertRule.getADAlertRecipientList()) {
          alert = OBProvider.getInstance().get(Alert.class);
          if (client != null) {
            alert.setClient(client);
          }
          if (organization != null) {
            alert.setOrganization(organization);
          }
          alert.setActive(true);
          alert.setAlertRule(alertRule);
          alert.setDescription(StringUtils.substring(description, 0, 2000));
          alert.setRecordID(recordId);
          alert.setReferenceSearchKey(referenceKeyId);
          alert.setRole(alertRecipient.getRole());
          alert.setUserContact(alertRecipient.getUserContact());
          alert.setAlertStatus(ALERT_STATUS_SOLVED);
          OBDal.getInstance().save(alert);
          // Send email
          if (alertRecipient.getUserContact() != null && alertRecipient.isSendEMail()
              && StringUtils.isNotEmpty(alertRecipient.getUserContact().getEmail())) {
            String linkToRecord = "";
            if (alertRule.getTab() != null) {
              linkToRecord = OBPropertiesProvider.getInstance().getOpenbravoProperties()
                  .getProperty("context.url")
                  + "/?Command=EDIT"
                  + "&tabId="
                  + alertRule.getTab().getId()
                  + "&recordId="
                  + referenceKeyId;
            }
            String moreText = "";
            if (StringUtils.isNotEmpty(description2)) {
              moreText += "\n\n" + description2;
            }
            if (StringUtils.isNotEmpty(tabId2) && StringUtils.isNotEmpty(recordId2)) {
              moreText += " "
                  + OBPropertiesProvider.getInstance().getOpenbravoProperties()
                      .getProperty("context.url") + "/?Command=EDIT" + "&tabId=" + tabId2
                  + "&recordId=" + recordId2;
            }
            String subject = translateAlertName(alertRule, alertRecipient.getUserContact()) + " "
                + recordId;
            String content = subject + ": " + description + " " + linkToRecord + moreText;
            sendEmailAlert(alertRecipient.getUserContact().getEmail(), subject, content);
          }
        }
      }
    }
    return alert;
  }

  public static String translateAlertName(AlertRule alertRule, User user) {
    String alertName = "";
    if (alertRule != null) {
      alertName = alertRule.getName();
      if (user != null && user.getDefaultLanguage() != null) {
        OBCriteria<AlertRuleTrl> obCriteria = OBDal.getInstance()
            .createCriteria(AlertRuleTrl.class);
        obCriteria.add(Restrictions.eq(AlertRuleTrl.PROPERTY_ALERTRULE, alertRule));
        obCriteria.add(Restrictions.eq(AlertRuleTrl.PROPERTY_LANGUAGE, user.getDefaultLanguage()));
        obCriteria.setFilterOnActive(false);
        AlertRuleTrl alertRuleTrl = (AlertRuleTrl) obCriteria.uniqueResult();
        if (alertRuleTrl != null) {
          alertName = alertRuleTrl.getName();
        }
      }
    }
    return alertName;
  }

  public static boolean sendEmailAlert(String recipientTO, String subject, String content) {
    boolean emailSent = false;
    if (StringUtils.isNotEmpty(recipientTO) && StringUtils.isNotEmpty(subject)
        && StringUtils.isNotEmpty(content)) {
      try {
        Organization currentOrg = OBContext.getOBContext().getCurrentOrganization();
        final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currentOrg);
        if (mailConfig != null) {
          final String username = mailConfig.getSmtpServerAccount();
          final String password = FormatUtilities.encryptDecrypt(
              mailConfig.getSmtpServerPassword(), false);
          final String connSecurity = mailConfig.getSmtpConnectionSecurity();
          final int port = mailConfig.getSmtpPort().intValue();
          final String senderAddress = mailConfig.getSmtpServerSenderAddress();
          final String host = mailConfig.getSmtpServer();
          final boolean auth = mailConfig.isSMTPAuthentification();
          EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
              recipientTO, null, null, username, subject, content, null, null, new Date(), null);
          emailSent = true;
        }
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    }
    return emailSent;
  }

  public static Note createNote(Tab tab, String record, String msg) {
    Note note = null;
    if (tab != null && StringUtils.isNotEmpty(record) && StringUtils.isNotEmpty(msg)) {
      note = OBProvider.getInstance().get(Note.class);
      note.setActive(true);
      note.setTable(tab.getTable());
      note.setRecord(record);
      note.setNote(StringUtils.substring(msg, 0, 4000));
      OBDal.getInstance().save(note);
    }
    return note;
  }
}