/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLConfigOutput;
import org.openbravo.externaldata.integration.process.ItemProcessor;
import org.openbravo.externaldata.integration.process.OutputDataProcessor;
import org.openbravo.service.db.DbUtility;

/**
 * The item processor of the Business Object Export EDL Process. It processes IDs to generate a list
 * of {@link SynchronizableBusinessObject}. This resulting list will be written into the output
 * using the {@link SynchronizableBusinessObjectExporter} defined in the context data.
 */
@Qualifier("Import_Master_Data")
public class ImportMasterDataItemProcessor extends ItemProcessor<String> {

  private static StringBuilder processResponse;

  private static String maxRecords = "10000";

  @Override
  protected void processItem(String item) throws OBException {
  }

  @Override
  protected boolean reprocessNotCommitedItemsOnException() {
    return true;
  }

  @Override
  public void outputExtraActions(OutputDataProcessor outputDataProcessor, OBEDLConfigOutput output) {
  }

  @Override
  public void writeResponse(Writer writer, OBEDLConfigOutput configOutput) throws IOException {
  }

  public static StringBuilder getProcessResponse() {
    if (processResponse == null) {
      processResponse = new StringBuilder();
    }
    return processResponse;
  }

  // Get list of records to import
  public static List<?> getRecordsToProcess(String table, String clientId, String adProcessRunId) {
    List<?> records = new ArrayList<>();
    if (StringUtils.isNotEmpty(table)) {
      StringBuilder hqlString = new StringBuilder();
      hqlString.append("SELECT e.id");
      hqlString.append(" FROM " + table + " e");
      hqlString.append(" WHERE e.processed = false");
      hqlString.append(" AND e.processRun.id = :processRunId");
      hqlString.append(" AND e.client.id = :clientId");
      Session session = OBDal.getInstance().getSession();
      Query query = session.createQuery(hqlString.toString());
      query.setMaxResults(Integer.parseInt(maxRecords));
      query.setParameter("processRunId", adProcessRunId);
      query.setParameter("clientId", clientId);
      records = query.list();
    }
    return records;
  }

  public static int updateRecordsProcessRunToCurrentProcessRun(ConnectionProvider conn,
      String clientId, String table, String columnName, String adProcessRunId)
      throws ServletException {
    int numRecords = ImportMasterDataUtilsData.updateStagingTableRecordsProcessRun(conn, table,
        adProcessRunId, columnName, clientId, maxRecords);
    return numRecords;
  }

  public static int updateRecordsProcessRunToNull(ConnectionProvider conn, String clientId,
      String table, String columnName, String adProcessRunId) throws ServletException {
    int numRecords = ImportMasterDataUtilsData.updateStagingTableRecordsProcessRunToNull(conn,
        table, columnName, adProcessRunId, clientId, maxRecords);
    return numRecords;
  }

  public static void doCommit() throws OBException {
    try {
      OBDal.getInstance().flush();
      SessionHandler.getInstance().getConnection().commit();
      OBDal.getInstance().getSession().clear();
    } catch (HibernateException | SQLException e) {
      // Some Database exceptions as constraint checks are thrown as HibernateException
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
      throw new OBException(message, e);
    }
  }
}
