/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import static com.openbravo.sharaf.integration.erpob.edl.IdBatchIterator.ID_SEPARATOR;

import java.util.Iterator;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.externaldata.integration.process.AsynchronousProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The processor used to export {@link SynchronizableBusinessObject} objects asynchronously.
 */
@Qualifier("OBSHINT_BaseOBObject")
public class BaseOBObjectAsynchronousProcessor extends AsynchronousProcessor<String, List<String>> {
  private static final Logger log = LoggerFactory
      .getLogger(BaseOBObjectAsynchronousProcessor.class);

  @Override
  protected Iterator<String> getDataBatcher() {
    List<String> ids = dataProcessor.getDataFromRaw();
    return new IdBatchIterator(ids, recordSize);
  }

  @Override
  protected String getBatchFromList(List<String> items) {
    StringBuilder errorIds = new StringBuilder();
    for (String id : items) {
      errorIds.append(id + ID_SEPARATOR);
    }
    return errorIds.toString();
  }

  @Override
  protected Iterator<String> getItemIterator() {
    String batch = requestLine.getLinedata();
    IdItemIterator itemIterator = new IdItemIterator(batch);
    log.debug("Processing batch with the following IDs:\n{}", batch);
    return itemIterator;
  }
}
