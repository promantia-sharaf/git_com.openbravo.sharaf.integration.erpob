/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * The class used to separate a list of IDs (Strings) in batches of a particular size.
 */
public class IdBatchIterator implements Iterator<String> {
  static final String ID_SEPARATOR = "\n";
  private List<String> idList;
  private int batchSize;
  private int position;
  private int length;

  public IdBatchIterator(List<String> idList, int batchSize) {
    this.idList = idList;
    this.batchSize = batchSize;
    this.position = 0;
    this.length = idList.size();
  }

  @Override
  public boolean hasNext() {
    return (position < length);
  }

  @Override
  public String next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }

    int partialLength = Math.min(position + batchSize, length);
    StringBuilder currentBatch = new StringBuilder();
    while (position < partialLength) {
      currentBatch.append(idList.get(position) + ID_SEPARATOR);
      position++;
    }
    return currentBatch.toString();
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}