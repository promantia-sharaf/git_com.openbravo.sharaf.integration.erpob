/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLConfigOutput;
import org.openbravo.externaldata.integration.process.ItemProcessor;
import org.openbravo.externaldata.integration.process.OutputDataProcessor;
import org.openbravo.service.db.DbUtility;

/**
 * The item processor of the Business Object Export EDL Process. It processes IDs to generate a list
 * of {@link SynchronizableBusinessObject}. This resulting list will be written into the output
 * using the {@link SynchronizableBusinessObjectExporter} defined in the context data.
 */
@Qualifier("Export_Sales_Data")
public class ExportSalesDataItemProcessor extends ItemProcessor<String> {

  private static StringBuilder processResponse;

  private static String maxRecords = "10000";

  @Override
  protected void processItem(String item) throws OBException {
  }

  @Override
  protected boolean reprocessNotCommitedItemsOnException() {
    return false;
  }

  @Override
  public void outputExtraActions(OutputDataProcessor outputDataProcessor, OBEDLConfigOutput output) {
  }

  @Override
  public void writeResponse(Writer writer, OBEDLConfigOutput configOutput) throws IOException {
  }

  public static StringBuilder getProcessResponse() {
    if (processResponse == null) {
      processResponse = new StringBuilder();
    }
    return processResponse;
  }

  // Get list of records to import
  public static List<?> getRecords(String table, String clientId, String adProcessRunId) {
    List<?> records = new ArrayList<>();
    if (StringUtils.isNotEmpty(table)) {
      StringBuilder hqlString = new StringBuilder();
      hqlString.append("SELECT e.id");
      hqlString.append(" FROM " + table + " e");
      hqlString.append(" , FIN_Payment_Sched_Ord_V p");
      hqlString.append(" WHERE e.id = p.salesOrder.id");
      hqlString.append(" AND e.obshintProcessrun = :processRunId");
      hqlString.append(" AND e.obshintIsexported = false");
      hqlString.append(" AND e.salesTransaction = true");
      hqlString.append(" AND (e.documentStatus = 'CO' or e.documentStatus = 'CL')");
      hqlString.append(" AND e.processed = true");
      hqlString.append(" AND e.transactionDocument.sOSubType != 'OB'");
      hqlString.append(" AND e.obposApplications != null");
      hqlString.append(" AND e.client.id = :clientId");
      hqlString.append(" AND e.obposIsDeleted = false");
      Session session = OBDal.getInstance().getSession();
      Query query = session.createQuery(hqlString.toString());
      query.setMaxResults(Integer.parseInt(maxRecords));
      query.setParameter("clientId", clientId);
      query.setParameter("processRunId", adProcessRunId);
      records = query.list();
    }
    return records;
  }

  public static int updateOrderRecordsProcessRunToCurrentProcessRun(ConnectionProvider conn,
      String clientId, String adProcessRunId) throws ServletException {
    int numRecords = ImportMasterDataUtilsData.updateOrderTableRecordsProcessRun(conn,
        adProcessRunId, clientId, maxRecords);
    return numRecords;
  }

  public static int updateOrderRecordsProcessRunToNull(ConnectionProvider conn, String clientId,
      String adProcessRunId) throws ServletException {
    int numRecords = ImportMasterDataUtilsData.updateOrderTableRecordsProcessRunToNull(conn,
        adProcessRunId, clientId, maxRecords);
    return numRecords;
  }

  public static void doCommit() throws OBException {
    try {
      OBDal.getInstance().flush();
      SessionHandler.getInstance().getConnection().commit();
      OBDal.getInstance().getSession().clear();
    } catch (HibernateException | SQLException e) {
      // Some Database exceptions as constraint checks are thrown as HibernateException
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
      throw new OBException(message, e);
    }
  }
}
