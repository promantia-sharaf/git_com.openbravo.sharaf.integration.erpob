/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;

import com.openbravo.sharaf.integration.erpob.process.ImportProductTaxCategory;

/**
 * The item processor of the Business Object Export EDL Process. It processes IDs to generate a list
 * of {@link SynchronizableBusinessObject}. This resulting list will be written into the output
 * using the {@link SynchronizableBusinessObjectExporter} defined in the context data.
 */
@Qualifier("Import_Product_TaxCategory")
public class ImportProductTaxCategoryItemProcessor extends ImportMasterDataItemProcessor {

  @Override
  protected void processItem(String item) throws OBException {
    ImportProductTaxCategory.createOrUpdateProductTaxCategory(item);
  }

}
