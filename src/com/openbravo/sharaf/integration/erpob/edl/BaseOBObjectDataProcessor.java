/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.externaldata.integration.process.DataProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The data processor used to export {@link SynchronizableBusinessObject} objects. It takes care of
 * retrieving the IDs of the objects to be exported. It also adds information as part of the context
 * data that later can be used by the item processors and the output data processors.
 */
@Qualifier("OBSHINT_BaseOBObject")
public class BaseOBObjectDataProcessor extends DataProcessor<List<String>> {

  public static final String ADDITIONAL_INFO = "additionalInfo";
  public static final String ENTITY_NAME = "entityName";
  public static final String EXPORTER = "exporter";
  public static final String IDS = "ids";

  private static final Logger log = LoggerFactory.getLogger(BaseOBObjectDataProcessor.class);

  @Override
  protected String getRawDataAsString() {
    if (processDataProcessor != null && processDataProcessor.isGetRawDataAsStringImplemented()) {
      return processDataProcessor.getRawDataAsString();
    }
    if (rawData instanceof List) {
      return toStringList((List<?>) rawData);
    }
    log.error("Unable to parse rawData to a String.");
    log.error("rawData instanceof {}", rawData.getClass());
    log.error("Content of rawData {}", rawData);
    throw new NotImplementedException();
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<String> getDataFromRaw() {
    return (List<String>) rawData;
  }

  public static String toStringList(List<?> rawData) {
    String result = "";
    for (Object s : rawData) {
      if (!StringUtils.isEmpty(result)) {
        result += ", ";
      }
      result += "'" + s + "'";
    }
    return result;
  }
}