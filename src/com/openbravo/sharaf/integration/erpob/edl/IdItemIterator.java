/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.edl;

import static com.openbravo.sharaf.integration.erpob.edl.IdBatchIterator.ID_SEPARATOR;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;

/**
 * The class used to iterate along a batch of IDs.
 */
public class IdItemIterator implements Iterator<String> {
  private String[] ids;
  private int length;
  private int position;

  public IdItemIterator(String batchOfIds) {
    if (StringUtils.isEmpty(batchOfIds)) {
      length = 0;
    } else {
      ids = batchOfIds.split(ID_SEPARATOR);
      length = ids.length;
    }
    position = 0;
  }

  @Override
  public boolean hasNext() {
    return (position < length);
  }

  @Override
  public String next() {
    if (position == length) {
      throw new NoSuchElementException();
    }
    return ids[position++];
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }

}