/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.priceadjustment.BusinessPartnerGroup;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.model.pricing.priceadjustment.PromotionType;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.OfferIn;
import com.openbravo.sharaf.integration.erpob.OfferlineIn;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;
import com.openbravo.sharaf.retail.discounts.OfferLimit;

public class ImportOfferIn extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PROMOTION_EDL_PROCESS_ID = "D7337CA83B6C40CC97B09CA9DFB4210D";
  public static final String TABLE_NAME = "obshint_offer_in";
  public static final String TABLE_COLUMN_ID = "obshint_offer_in_id";
  static OBEDLRequest edlRequest = null;

  public static final String PRICE_ADJUSTMENT_PROMO_TYPE_ID = "CA5491E6000647BD889B8D7CDF680795";
  public static final String PROMOTION_PROMO_TYPE_ID = "6A3C2313136147A6B2D1B8D2F5F768E6";
  public static final String OB_PRICEADJUSTMENT_TYPE_ID = "5D4BAF6BB86D4D2C9ED3D5A6FC051579";
  public static final String CART_LEVEL_PROMO_TYPE_ID = "61D65EECCF97487A82D169BB7E96832C";
  public static final String BUYXPAYY_PROMO_TYPE_ID = "1424B47A341E4D38B0F61CCF26450AF3";

  static final String SAP_DISCOUNT_PROMO_TYPE = "D";
  static final String ONLY_THOSE_DEFINED = "N";
  private static Logger log = Logger.getLogger(ImportOfferIn.class);

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PROMOTION_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.log(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdatePromotion(String item) {
    OfferIn stagingPromotion = OBDal.getInstance().get(OfferIn.class, item);
    try {
      if (stagingPromotion != null) {
        boolean save = false;
        PriceAdjustment mOffer = getOffer(stagingPromotion);
        if (mOffer == null) {
          mOffer = OBProvider.getInstance().get(PriceAdjustment.class);
          mOffer.setId(stagingPromotion.getId());
          mOffer.setNewOBObject(true);
          save = true;
        }
        updatePromotion(mOffer, stagingPromotion, save);
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static void updatePromotion(PriceAdjustment mOffer, OfferIn stagingPromotion, boolean save) {

    // Update promotion type
    String promoType = null;
    List<OfferlineIn> sharafLines = stagingPromotion.getObshintOfferlineInList();

    log.debug("sharaflines.size is "+sharafLines.size());
    log.debug("primary sku is "+stagingPromotion.getPrimarySku());
    if(!sharafLines.isEmpty()){
        log.debug("secondary sku is "+sharafLines.get(0).getSecondarySku());
    }
    if (SAP_DISCOUNT_PROMO_TYPE.equals(stagingPromotion.getType())) {
      promoType = OB_PRICEADJUSTMENT_TYPE_ID;
      mOffer.setDiscount(stagingPromotion.getPercentage());
      mOffer.setDiscountAmount(stagingPromotion.getAmount());
    } else if (sharafLines.size() == 0) {
      promoType = PRICE_ADJUSTMENT_PROMO_TYPE_ID;
    } else if(sharafLines.size() == 1 && sharafLines.get(0).getSecondarySku().equals(stagingPromotion.getPrimarySku())){
      promoType = BUYXPAYY_PROMO_TYPE_ID;
    } else if(stagingPromotion.getTotalMinimumAmount() == null || stagingPromotion.getTotalMinimumAmount().compareTo(new BigDecimal(0)) <= 0){
      promoType = PROMOTION_PROMO_TYPE_ID;
    } else{
      promoType = CART_LEVEL_PROMO_TYPE_ID;
    }

    log.debug("promotype is "+promoType);
    mOffer.setDiscountType(OBDal.getInstance().get(PromotionType.class, promoType));

    // Set common properties
    mOffer.setClient(stagingPromotion.getClient());
    mOffer.setOrganization(stagingPromotion.getOrganization());
    mOffer.setActive(stagingPromotion.isActive());
    mOffer.setName(stagingPromotion.getPromo());
    mOffer.setPrintName(stagingPromotion.getERPPromo());
    mOffer.setStartingDate(stagingPromotion.getPromoStart());
    mOffer.setEndingDate(stagingPromotion.getPromoEnd());
    if(promoType == CART_LEVEL_PROMO_TYPE_ID){
    	mOffer.setCustdisTotalMinAmount(stagingPromotion.getTotalMinimumAmount());
    	mOffer.setCustdisIsBuyAnd(stagingPromotion.isBuyAnd());
    	mOffer.setCustdisIsGetAnd(stagingPromotion.isGetAnd());
    }
    if(promoType == BUYXPAYY_PROMO_TYPE_ID){
    	mOffer.setCustdisX((long) 1);
    	mOffer.setCustdisY((long) 1);
    }
    mOffer.setIncludedProducts(ONLY_THOSE_DEFINED);
    if(stagingPromotion.getBusinessPartnerCategoryName() != null && stagingPromotion.getBusinessPartnerCategoryName() != ""){
        mOffer.setIncludedBPCategories(ONLY_THOSE_DEFINED);
    }
    mOffer.setObshintPromoDocno(stagingPromotion.getPromoDocno());
    mOffer.setObshintPromoDesc(stagingPromotion.getPromoDesc());
    mOffer.setObshintPrimarylink(stagingPromotion.getPrimaryLink());
    mOffer.setApplyNext(false);
    if(stagingPromotion.getSellableLimit() != null){
    mOffer.setCustdisSellableLimit(stagingPromotion.getSellableLimit());
    }
    if (save) {
      OBDal.getInstance().save(mOffer);
    }
    if (stagingPromotion.getSellableLimit() != null) {
      createOrUpdateOfferLimitTable(mOffer, stagingPromotion);
    }
    if(stagingPromotion.getBusinessPartnerCategoryName() != null && stagingPromotion.getBusinessPartnerCategoryName() != ""){
        // Updating Business Partner Category Lines
        List<BusinessPartnerGroup> offerBPCategoryList = mOffer
        	.getPricingAdjustmentBusinessPartnerGroupList();
        if(offerBPCategoryList.size() != 0){
            for (BusinessPartnerGroup offerBPCategory : offerBPCategoryList) {
            	offerBPCategory.setActive(false);
            }
        }

        Category bpCategory = getBPGroupByName(stagingPromotion.getBusinessPartnerCategoryName());
        BusinessPartnerGroup offerBusinessPartnerGroup = getOfferBPCategory(bpCategory, mOffer);
        if (offerBusinessPartnerGroup == null) {
        	offerBusinessPartnerGroup = OBProvider.getInstance().get(
	          BusinessPartnerGroup.class);
        	offerBusinessPartnerGroup.setNewOBObject(true);
        	offerBusinessPartnerGroup.setBusinessPartnerCategory(bpCategory);
            offerBusinessPartnerGroup.setClient(mOffer.getClient());
            offerBusinessPartnerGroup.setOrganization(mOffer.getOrganization());
            offerBusinessPartnerGroup.setPriceAdjustment(mOffer);
        }
        offerBusinessPartnerGroup.setActive(true);
        OBDal.getInstance().save(offerBusinessPartnerGroup);
    }

    // Updating Products lines
    List<org.openbravo.model.pricing.priceadjustment.Product> products = mOffer
        .getPricingAdjustmentProductList();
    products.clear();

    List<String> productIdList = new ArrayList<String>();
    productIdList.add(stagingPromotion.getPrimarySku());
    for (OfferlineIn oLine : sharafLines) {
      productIdList.add(oLine.getSecondarySku());
    }
    // Check duplicate product lines
    if(promoType != BUYXPAYY_PROMO_TYPE_ID){
    	log.debug("calling checkduplicateproducts for promotype "+promoType);
    	checkDuplicateProducts(productIdList, stagingPromotion);
    }
    // Check for buy1 get1 promotion
    if(promoType == BUYXPAYY_PROMO_TYPE_ID){
    	log.debug("calling checkbuy1get1promo for promotype "+promoType);
    	checkbuy1get1promo(sharafLines, stagingPromotion);
    }

    // Primary line
    org.openbravo.model.pricing.priceadjustment.Product offerProduct;
    if(promoType != CART_LEVEL_PROMO_TYPE_ID){
        Product primaryProduct = getProductByCode(stagingPromotion.getPrimarySku());
        offerProduct = getOfferProduct(
            primaryProduct, mOffer, null);
        offerProduct.setProduct(primaryProduct);
        offerProduct.setPriceAdjustment(mOffer);
        offerProduct.setSapsharSapcode(stagingPromotion.getSAPCode());
        offerProduct.setClient(mOffer.getClient());
        offerProduct.setOrganization(mOffer.getOrganization());
        offerProduct.setCustdisAmount(stagingPromotion.getTotalAmount());
        offerProduct.setCustdisDiscountamount(stagingPromotion.getAmount());
        offerProduct.setCustdisPercentage(stagingPromotion.getPercentage());
        offerProduct.setActive(stagingPromotion.isActive());
        products.add(offerProduct);
        OBDal.getInstance().save(offerProduct);
    }
    // Secondary Lines
    if(promoType != BUYXPAYY_PROMO_TYPE_ID){
    for (OfferlineIn oLine : sharafLines) {
      Product secondaryProduct = getProductByCode(oLine.getSecondarySku());
      offerProduct = getOfferProduct(secondaryProduct, mOffer, oLine);
      if(promoType == CART_LEVEL_PROMO_TYPE_ID){
          offerProduct.setCustdisIsGift(oLine.isGiftProduct());
      } else{
          offerProduct.setCustdisIsGift(true);
      }
      offerProduct.setCustdisGiftqty(oLine.getQuantity());
      offerProduct.setProduct(secondaryProduct);
      offerProduct.setPriceAdjustment(mOffer);
      offerProduct.setClient(oLine.getClient());
      offerProduct.setOrganization(oLine.getOrganization());
      if (secondaryProduct.getCustshaIrType() != null) {
        offerProduct.setCustdisIrType(secondaryProduct.getCustshaIrType());
        offerProduct.setCustdisAmount(secondaryProduct.getCustshaIrAmount());
      } else {
        offerProduct.setCustdisAmount(oLine.getSecondarySkuPrice());
      }
      offerProduct.setCustdisDiscountamount(oLine.getCardAmount());
      offerProduct.setCustdisPercentage(oLine.getPercentage());
      offerProduct.setSapsharSapcode(oLine.getSAPCode());
      offerProduct.setActive(oLine.isActive());
      products.add(offerProduct);
      OBDal.getInstance().save(offerProduct);
      oLine.setProcessed(true);
    }
    }
    mOffer.setPricingAdjustmentProductList(products);
    stagingPromotion.setProcessed(true);
    stagingPromotion.setProcessRun(null);
  }

private static void createOrUpdateOfferLimitTable(PriceAdjustment mOffer, OfferIn stagingPromotion) {
    OfferLimit promotionOfferLimit = null;
    OBCriteria<OfferLimit> promotionLimit = OBDal.getInstance().createCriteria(OfferLimit.class);
    promotionLimit.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, mOffer.getClient()));
    promotionLimit.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, mOffer));

    if (promotionLimit.list().isEmpty()) {
      promotionOfferLimit = OBProvider.getInstance().get(OfferLimit.class);
      promotionOfferLimit.setActive(true);
      promotionOfferLimit.setClient(stagingPromotion.getClient());
      promotionOfferLimit.setPromotionDiscount(mOffer);
      OBDal.getInstance().save(promotionOfferLimit);
    }

  }

public static PriceAdjustment getOffer(OfferIn stagingPromotion) {
    String promoId = stagingPromotion.getPromo();
    String primaryLink = stagingPromotion.getPrimaryLink();
    Product primaryProduct = null;
    if(stagingPromotion.getTotalMinimumAmount() == null || stagingPromotion.getTotalMinimumAmount().compareTo(new BigDecimal(0)) <= 0){
    	primaryProduct = getProductByCode(stagingPromotion.getPrimarySku());
    }

    StringBuffer where = new StringBuffer();
    where.append(" select pa." + PriceAdjustment.PROPERTY_ID + " as id ");
    where.append(" from " + org.openbravo.model.pricing.priceadjustment.Product.ENTITY_NAME
        + " pap");
    where.append(" join pap."
        + org.openbravo.model.pricing.priceadjustment.Product.PROPERTY_PRICEADJUSTMENT + " pa");
    if(primaryProduct != null){
    	where.append("   where pap."
            + org.openbravo.model.pricing.priceadjustment.Product.PROPERTY_PRODUCT + ".id = :productId");
    } else if(stagingPromotion.getTotalMinimumAmount() != null){
    	where.append("   where pa."
                + PriceAdjustment.PROPERTY_CUSTDISTOTALMINAMOUNT + ".custdisTotalMinAmount = :custdisTotalMinAmount");
    }
    where.append("   and pap."
        + org.openbravo.model.pricing.priceadjustment.Product.PROPERTY_CUSTDISISGIFT + " = false");
    where.append("   and pa." + PriceAdjustment.PROPERTY_OBSHINTPRIMARYLINK + " = :primaryLinkId");
    where.append("   and pa." + PriceAdjustment.PROPERTY_NAME + " = :promoId");
    where.append("   and pa." + PriceAdjustment.PROPERTY_ORGANIZATION + ".id = :orgId");

    Query offerQry = OBDal.getInstance().getSession().createQuery(where.toString());
    if(primaryProduct != null){
    	offerQry.setParameter("productId", primaryProduct.getId());
    } else if(stagingPromotion.getTotalMinimumAmount() != null){
    	offerQry.setParameter("custdisTotalMinAmount", stagingPromotion.getTotalMinimumAmount());
    }
    offerQry.setParameter("primaryLinkId", primaryLink);
    offerQry.setParameter("promoId", promoId);
    offerQry.setParameter("orgId", stagingPromotion.getOrganization().getId());

    @SuppressWarnings("unchecked")
    List<PriceAdjustment> offerQryList = offerQry.list();
    if (offerQryList.size() == 1) {
      PriceAdjustment mOffer = OBDal.getInstance().get(PriceAdjustment.class, offerQryList.get(0));
      return mOffer;
    } else if (offerQryList.size() > 1) {
      throw new OBException(String.format(OBMessageUtils
          .messageBD("OBSHINT_PROMOTION_CREATION_ERROR"), stagingPromotion.getOrganization()
          .getSearchKey(), promoId, primaryLink, primaryProduct != null ? primaryProduct.getSearchKey(): ""));
    } else {
      return null;
    }
  }

  public static Product getProductByCode(String pcode) {
    Product product = null;
    OBCriteria<Product> obCriteria = OBDal.getInstance().createCriteria(Product.class);
    obCriteria.add(Restrictions.eq(Product.PROPERTY_SEARCHKEY, pcode));
    obCriteria.setFilterOnActive(false);
    product = (Product) obCriteria.uniqueResult();
    if (product == null) {
      throw new OBException(String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
          pcode));
    }
    return product;
  }

  private static Category getBPGroupByName(String businessPartnerCategoryName) {
	Category bpGroup = null;
	OBCriteria<Category> obCriteria = OBDal.getInstance().createCriteria(Category.class);
	obCriteria.add(Restrictions.eq(Category.PROPERTY_NAME, businessPartnerCategoryName));
	bpGroup = (Category) obCriteria.uniqueResult();
    if (bpGroup == null) {
        throw new OBException(String.format(OBMessageUtils.messageBD("OBSHINT_BUSINESS_PARTNER_CATEGORY_NOT_FOUND"),
        		businessPartnerCategoryName));
    }
	return bpGroup;  
  }
  
  public static org.openbravo.model.pricing.priceadjustment.Product getOfferProduct(
      Product primaryProduct, PriceAdjustment mOffer, OfferlineIn oLine) {
    org.openbravo.model.pricing.priceadjustment.Product offerProduct = null;
    OBCriteria<org.openbravo.model.pricing.priceadjustment.Product> obCriteria = OBDal
        .getInstance().createCriteria(org.openbravo.model.pricing.priceadjustment.Product.class);
    obCriteria.add(Restrictions.eq(
        org.openbravo.model.pricing.priceadjustment.Product.PROPERTY_PRODUCT, primaryProduct));
    obCriteria.add(Restrictions.eq(
        org.openbravo.model.pricing.priceadjustment.Product.PROPERTY_PRICEADJUSTMENT, mOffer));
    obCriteria.setFilterOnActive(false);
    offerProduct = (org.openbravo.model.pricing.priceadjustment.Product) obCriteria.uniqueResult();
    if (offerProduct == null) {
      offerProduct = OBProvider.getInstance().get(
          org.openbravo.model.pricing.priceadjustment.Product.class);
      offerProduct.setId(oLine != null ? oLine.getId() : mOffer.getId());
      offerProduct.setNewOBObject(true);
    }
    return offerProduct;
  }

  private static BusinessPartnerGroup getOfferBPCategory(Category category, PriceAdjustment mOffer) {
	    OBCriteria<BusinessPartnerGroup> bpGroupCriteria = OBDal
	        .getInstance().createCriteria(BusinessPartnerGroup.class);
	    bpGroupCriteria.add(Restrictions.eq(BusinessPartnerGroup.PROPERTY_BUSINESSPARTNERCATEGORY, category));
	    bpGroupCriteria.add(Restrictions.eq(BusinessPartnerGroup.PROPERTY_PRICEADJUSTMENT, mOffer));
	    bpGroupCriteria.setFilterOnActive(true);
	    return (BusinessPartnerGroup) bpGroupCriteria.uniqueResult();
  }
  
  public static void checkDuplicateProducts(List<String> productIdList, OfferIn stagingPromotion) {

    final Set<String> setToReturn = new HashSet<String>();
    final Set<String> set1 = new HashSet<String>();

    for (String yourInt : productIdList) {
      if (!set1.add(yourInt)) {
        setToReturn.add(yourInt);
      }
    }
    if (setToReturn.size() > 0) {
      throw new OBException(String.format(
          OBMessageUtils.messageBD("OBSHINT_PROMOTION_DUPLICATE_LINES"),
          stagingPromotion.getPrimaryLink()));
    }
  }

  private static void checkbuy1get1promo(List<OfferlineIn> sharafLines, OfferIn stagingPromotion) {
	  if(sharafLines.get(0).getPercentage() == null || sharafLines.get(0).getSecondarySkuPrice() == null) {
		  throw new OBException("Secondary SKU promo price or Secondary article discount cannot be null for Sharaf Buy X pay Y promotion type for promotion "+ stagingPromotion.getPrimaryLink());
	  } else {
		  if(!(sharafLines.get(0).getPercentage().compareTo(new BigDecimal(100)) == 0)){
			  throw new OBException("Secondary article discount must be 100% for Sharaf Buy X pay Y promotion type for promotion "+ stagingPromotion.getPrimaryLink());
		  }else if(!(sharafLines.get(0).getSecondarySkuPrice().compareTo(new BigDecimal(0)) == 0)){
			  throw new OBException("Secondary SKU promo price must be 0 for Sharaf Buy X pay Y promotion type for promotion "+ stagingPromotion.getPrimaryLink());
		  }
	  }

	  //Throw error if secondary article quantity is not equal to 1
	  if (sharafLines.get(0).getQuantity() != 1) {
		  throw new OBException("Secondary article Quantity must be 1 for Sharaf Buy X pay Y promotion type for promotion " + stagingPromotion.getPrimaryLink());
	  }
  }
}