/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.plm.Product;
import org.openbravo.multiupc.MultiUPC;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.ProductBarcode;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportProductBarcode extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_BARCODE_EDL_PROCESS_ID = "5713DD98BCDD4A22B51A8D2224007532";
  public static final String TABLE_NAME = "obshint_product_barcode";
  public static final String TABLE_COLUMN_ID = "obshint_product_barcode_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_BARCODE_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProductBarcode(String item) {
    String msg = "";
    ProductBarcode stagingProductBarcode = OBDal.getInstance().get(ProductBarcode.class, item);
    try {
      if (stagingProductBarcode != null) {
        boolean save = false;
        Product product = ImportProduct.getProductBySearchKey(stagingProductBarcode.getSku());
        if (product != null) {
          String upc = stagingProductBarcode.getUpc();
          if (StringUtils.isNotEmpty(upc)) {
            MultiUPC multiUPC = getMultiUPC(upc);
            if (multiUPC == null) {
              multiUPC = OBProvider.getInstance().get(MultiUPC.class);
              multiUPC.setId(stagingProductBarcode.getId());
              multiUPC.setNewOBObject(true);
              save = true;
            }
            multiUPC.setClient(stagingProductBarcode.getClient());
            multiUPC.setOrganization(stagingProductBarcode.getOrganization());
            multiUPC.setActive(stagingProductBarcode.isActive());
            multiUPC.setProduct(product);
            multiUPC.setUpc(upc);
            if (save) {
              OBDal.getInstance().save(multiUPC);
            }
            stagingProductBarcode.setProcessed(true);
            stagingProductBarcode.setProcessRun(null);
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
              stagingProductBarcode.getSku());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static MultiUPC getMultiUPC(String upc) {
    MultiUPC multiUPC = null;
    if (StringUtils.isNotEmpty(upc)) {
      OBCriteria<MultiUPC> obCriteria = OBDal.getInstance().createCriteria(MultiUPC.class);
      obCriteria.add(Restrictions.eq(MultiUPC.PROPERTY_UPC, upc));
      obCriteria.setFilterOnActive(false);
      multiUPC = (MultiUPC) obCriteria.uniqueResult();
    }
    return multiUPC;
  }

}