/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.financial.multitaxcategory.Product_TaxCategory;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.ProductTaxCategory;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportProductTaxCategory extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_TAXCATEGORY_EDL_PROCESS_ID = "02D5412D9C1645BF90C13EB7CE775488";
  public static final String TABLE_NAME = "obshint_product_taxcategory";
  public static final String TABLE_COLUMN_ID = "obshint_product_taxcategory_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_TAXCATEGORY_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProductTaxCategory(String item) {
    String msg = "";
    ProductTaxCategory stagingProductTaxCategory = OBDal.getInstance().get(
        ProductTaxCategory.class, item);
    try {
      if (stagingProductTaxCategory != null) {
        boolean save = false;
        Product product = ImportProduct.getProductBySearchKey(stagingProductTaxCategory.getSKU());
        if (product == null) {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
              stagingProductTaxCategory.getSKU());
          throw new OBException(msg);
        }
        TaxCategory taxCategory = ImportProduct.getTaxCategoryByName(stagingProductTaxCategory
            .getTAXCategory());
        if (taxCategory == null) {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_TAX_CATEGORY_NOT_FOUND"),
              stagingProductTaxCategory.getTAXCategory());
          throw new OBException(msg);
        }
        Product_TaxCategory productTaxCategory = getProductTaxCategoryRecord(
            stagingProductTaxCategory.getOrganization(), product, taxCategory);
        if (productTaxCategory == null) {
          productTaxCategory = OBProvider.getInstance().get(Product_TaxCategory.class);
          productTaxCategory.setId(stagingProductTaxCategory.getId());
          productTaxCategory.setNewOBObject(true);
          save = true;
        }
        productTaxCategory.setClient(stagingProductTaxCategory.getClient());
        productTaxCategory.setOrganization(stagingProductTaxCategory.getOrganization());
        productTaxCategory.setActive(stagingProductTaxCategory.isActive());
        productTaxCategory.setProduct(product);
        productTaxCategory.setTaxCategory(taxCategory);
        if (save) {
          OBDal.getInstance().save(productTaxCategory);
        }
        stagingProductTaxCategory.setProcessed(true);
        stagingProductTaxCategory.setProcessRun(null);
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static Product_TaxCategory getProductTaxCategoryRecord(Organization organization,
      Product product, TaxCategory taxCategory) {
    OBCriteria<Product_TaxCategory> criteria = OBDal.getInstance().createCriteria(
        Product_TaxCategory.class);
    criteria.add(Restrictions.eq(Product_TaxCategory.PROPERTY_ORGANIZATION, organization));
    criteria.add(Restrictions.eq(Product_TaxCategory.PROPERTY_PRODUCT, product));
    criteria.add(Restrictions.eq(Product_TaxCategory.PROPERTY_TAXCATEGORY, taxCategory));
    criteria.setMaxResults(1);
    if (criteria.uniqueResult() != null) {
      return (Product_TaxCategory) criteria.uniqueResult();
    } else {
      return null;
    }
  }
}