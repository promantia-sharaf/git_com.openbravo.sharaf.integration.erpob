/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.sharaf.integration.erpob.process;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.handlers.RequestReprocessHandler;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.service.json.JsonConstants;

import com.openbravo.sharaf.integration.erpob.Assortment;
import com.openbravo.sharaf.integration.erpob.Brand;
import com.openbravo.sharaf.integration.erpob.BusinessPartner;
import com.openbravo.sharaf.integration.erpob.OfferIn;
import com.openbravo.sharaf.integration.erpob.Product;
import com.openbravo.sharaf.integration.erpob.ProductBarcode;
import com.openbravo.sharaf.integration.erpob.ProductCategory;
import com.openbravo.sharaf.integration.erpob.ProductPrice;
import com.openbravo.sharaf.integration.erpob.ProductStock;
import com.openbravo.sharaf.integration.erpob.ProductTaxCategory;

public class ReprocessRecord extends RequestReprocessHandler {
  private static final Logger log = Logger.getLogger(ReprocessRecord.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    try {
      JSONObject result = new JSONObject();

      JSONObject request = new JSONObject(content);
      String inpTableId = request.getString("inpTableId");
      if (StringUtils.isNotEmpty(inpTableId)) {
        Table table = OBDal.getInstance().get(Table.class, inpTableId);
        if (table != null) {
          String requestId = "";
          String recordId = "";
          switch (table.getDBTableName()) {
          case ImportBrand.TABLE_NAME:
            recordId = request.getString("inpobshintBrandId");
            Brand stagingBrand = OBDal.getInstance().get(Brand.class, recordId);
            if (stagingBrand != null && stagingBrand.getObedlRequest() != null) {
              requestId = stagingBrand.getObedlRequest().getId();
            }
            break;
          case ImportProductCategory.TABLE_NAME:
            recordId = request.getString("inpobshintProductCategoryId");
            ProductCategory stagingProductCategory = OBDal.getInstance().get(ProductCategory.class,
                recordId);
            if (stagingProductCategory != null && stagingProductCategory.getObedlRequest() != null) {
              requestId = stagingProductCategory.getObedlRequest().getId();
            }
            break;
          case ImportProduct.TABLE_NAME:
            recordId = request.getString("inpobshintProductId");
            Product stagingProduct = OBDal.getInstance().get(Product.class, recordId);
            if (stagingProduct != null && stagingProduct.getObedlRequest() != null) {
              requestId = stagingProduct.getObedlRequest().getId();
            }
            break;
          case ImportProductBarcode.TABLE_NAME:
            recordId = request.getString("inpobshintProductBarcodeId");
            ProductBarcode stagingProductBarcode = OBDal.getInstance().get(ProductBarcode.class,
                recordId);
            if (stagingProductBarcode != null && stagingProductBarcode.getObedlRequest() != null) {
              requestId = stagingProductBarcode.getObedlRequest().getId();
            }
            break;
          case ImportProductPrice.TABLE_NAME:
            recordId = request.getString("inpobshintProductPriceId");
            ProductPrice stagingProductPrice = OBDal.getInstance()
                .get(ProductPrice.class, recordId);
            if (stagingProductPrice != null && stagingProductPrice.getObedlRequest() != null) {
              requestId = stagingProductPrice.getObedlRequest().getId();
            }
            break;
          case ImportProductStock.TABLE_NAME:
            recordId = request.getString("inpobshintProductStockId");
            ProductStock stagingProductStock = OBDal.getInstance()
                .get(ProductStock.class, recordId);
            if (stagingProductStock != null && stagingProductStock.getObedlRequest() != null) {
              requestId = stagingProductStock.getObedlRequest().getId();
            }
            break;
          case ImportProductAssortment.TABLE_NAME:
            recordId = request.getString("inpobshintAssortmentId");
            Assortment stagingAssortment = OBDal.getInstance().get(Assortment.class, recordId);
            if (stagingAssortment != null && stagingAssortment.getObedlRequest() != null) {
              requestId = stagingAssortment.getObedlRequest().getId();
            }
            break;
          case ImportBusinessPartner.TABLE_NAME:
            recordId = request.getString("inpobshintBpartnerId");
            BusinessPartner stagingBusinessPartner = OBDal.getInstance().get(BusinessPartner.class,
                recordId);
            if (stagingBusinessPartner != null && stagingBusinessPartner.getObedlRequest() != null) {
              requestId = stagingBusinessPartner.getObedlRequest().getId();
            }
            break;
          case ImportOfferIn.TABLE_NAME:
            recordId = request.getString("inpobshintOfferInId");
            OfferIn stagingPromotion = OBDal.getInstance().get(OfferIn.class, recordId);
            if (stagingPromotion != null && stagingPromotion.getObedlRequest() != null) {
              requestId = stagingPromotion.getObedlRequest().getId();
            }
            break;
          case ImportProductTaxCategory.TABLE_NAME:
            recordId = request.getString("inpobshintProductTaxcategoryId");
            ProductTaxCategory stagingProductTaxCategory = OBDal.getInstance().get(
                ProductTaxCategory.class, recordId);
            if (stagingProductTaxCategory != null
                && stagingProductTaxCategory.getObedlRequest() != null) {
              requestId = stagingProductTaxCategory.getObedlRequest().getId();
            }
            break;
          default:
            break;
          }
          if (StringUtils.isNotEmpty(requestId)) {
            boolean errorsOnly = true;
            ProcessRequest<?, ?> processRequest = ProcessRequest.getProcessRequestInstance();
            JSONObject responseJson = processRequest.reprocessRequest(requestId, errorsOnly);
            JSONObject viewMsg = new JSONObject();
            if (responseJson.getBoolean(JsonConstants.RESPONSE_ERROR)) {
              viewMsg.put("msgType", "error");
            } else {
              viewMsg.put("msgType", "success");
            }
            viewMsg.put("msgTitle", OBMessageUtils.messageBD("obedl_reprocess_title"));
            viewMsg.put("msgText", responseJson.getString(JsonConstants.RESPONSE_ERRORMESSAGE));

            JSONObject viewMsgAction = new JSONObject();
            viewMsgAction.put("showMsgInView", viewMsg);
            // Execute process and prepare an array with actions to be executed after execution
            JSONArray actions = new JSONArray();
            actions.put(viewMsgAction);
            result.put("responseActions", actions);
          }
        }
      }
      return result;
    } catch (JSONException e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }

}