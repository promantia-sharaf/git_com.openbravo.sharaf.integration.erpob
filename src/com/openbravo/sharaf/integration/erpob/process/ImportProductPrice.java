/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.Date;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.retail.posterminal.POSUtils;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.ProductPrice;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportProductPrice extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_PRICE_EDL_PROCESS_ID = "2D00529CE2844E4D8E48A3F67035D5E9";
  public static final String TABLE_NAME = "obshint_product_price";
  public static final String TABLE_COLUMN_ID = "obshint_product_price_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_PRICE_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProductPrice(String item) {
    String msg = "";
    ProductPrice stagingProductPrice = OBDal.getInstance().get(ProductPrice.class, item);
    try {
      if (stagingProductPrice != null) {
        boolean save = false;
        Product product = ImportProduct.getProductBySearchKey(stagingProductPrice.getSKU());
        if (product != null) {
          PriceListVersion salesPriceListVersion = null;
          try {
            salesPriceListVersion = getSalesPriceListVersion(stagingProductPrice.getOrganization(),
                stagingProductPrice.getCreationDate());
          } catch (Exception e) {
            salesPriceListVersion = null;
          }
          if (salesPriceListVersion != null) {
            org.openbravo.model.pricing.pricelist.ProductPrice productPrice = getProductPriceByProductAndPriceListVersion(
                product, salesPriceListVersion);
            if (productPrice == null) {
              productPrice = OBProvider.getInstance().get(
                  org.openbravo.model.pricing.pricelist.ProductPrice.class);
              productPrice.setId(stagingProductPrice.getId());
              productPrice.setNewOBObject(true);
              save = true;
            }
            productPrice.setClient(stagingProductPrice.getClient());
            productPrice.setOrganization(stagingProductPrice.getOrganization());
            productPrice.setActive(stagingProductPrice.isActive());
            productPrice.setProduct(product);
            productPrice.setPriceListVersion(salesPriceListVersion);
            productPrice.setStandardPrice(stagingProductPrice.getPrice());
            productPrice.setListPrice(stagingProductPrice.getPrice());
            if (save) {
              OBDal.getInstance().save(productPrice);
            }
            stagingProductPrice.setProcessed(true);
            stagingProductPrice.setProcessRun(null);
          } else {
            msg = String.format(
                OBMessageUtils.messageBD("OBSHINT_SALES_PRICE_LIST_VERSION_NOT_FOUND"),
                stagingProductPrice.getOrganization().getIdentifier());
            throw new OBException(msg);
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
              stagingProductPrice.getSKU());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static org.openbravo.model.pricing.pricelist.ProductPrice getProductPriceByProductAndPriceListVersion(
      Product product, PriceListVersion salesPriceListVersion) {
    org.openbravo.model.pricing.pricelist.ProductPrice productPrice = null;
    if (product != null) {
      OBCriteria<org.openbravo.model.pricing.pricelist.ProductPrice> obCriteria = OBDal
          .getInstance().createCriteria(org.openbravo.model.pricing.pricelist.ProductPrice.class);
      obCriteria.add(Restrictions.eq(
          org.openbravo.model.pricing.pricelist.ProductPrice.PROPERTY_PRODUCT, product));
      obCriteria.add(Restrictions.eq(
          org.openbravo.model.pricing.pricelist.ProductPrice.PROPERTY_PRICELISTVERSION,
          salesPriceListVersion));
      obCriteria.setFilterOnActive(false);
      productPrice = (org.openbravo.model.pricing.pricelist.ProductPrice) obCriteria.uniqueResult();
    }
    return productPrice;
  }

  public static PriceListVersion getSalesPriceListVersion(Organization organization, Date date) {
    PriceListVersion salesPriceListVersion = null;
    if (organization != null) {
      salesPriceListVersion = POSUtils.getPriceListVersionByOrgId(organization.getId(), date);
      if (salesPriceListVersion != null && !salesPriceListVersion.getPriceList().isSalesPriceList()) {
        salesPriceListVersion = null;
      }
    }
    return salesPriceListVersion;
  }

}