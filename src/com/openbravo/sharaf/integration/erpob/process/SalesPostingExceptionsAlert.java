package com.openbravo.sharaf.integration.erpob.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.common.order.Order;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

import com.openbravo.sharaf.integration.erpob.SalesPostingExceptions;
import com.openbravo.sharaf.integration.erpob.utility.AlertUtils;

public class SalesPostingExceptionsAlert extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String SALES_POSTING_EXCEPTIONS_TAB_ID = "B21BB1FA9CEF47D3A685E568EBDE3C35";

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();

    try {
      OBContext.setAdminMode(true);
      String msg = OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_ALERTING_COUNT");
      List<SalesPostingExceptions> salesPostingExceptionsToAlert = getSalesPostingExceptionsToAlert();
      logger.logln(msg + ": " + salesPostingExceptionsToAlert.size());
      for (SalesPostingExceptions salesPostingExceptionToAlert : salesPostingExceptionsToAlert) {
        Order salesOrder = ExportInvoice.getOrderByInvoiceReference(salesPostingExceptionToAlert
            .getInvoiceReference());
        String strNotFound = "";
        if (salesOrder != null) {
          createSendAlert(salesOrder, String.format(
              OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_ALERTING_SALES_ORDER"),
              salesOrder.getIdentifier(), salesPostingExceptionToAlert.getErrorMessage()),
              String.format(
                  OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_ALERTING_FIX_DATE"),
                  salesOrder.getIdentifier()), SALES_POSTING_EXCEPTIONS_TAB_ID,
              salesPostingExceptionToAlert.getId());
          salesPostingExceptionToAlert.setAlerted(true);
        } else {
          strNotFound = ": "
              + OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_NO_ORDER_FOUND");
          AlertUtils.createNote(
              OBDal.getInstance().get(Tab.class, SALES_POSTING_EXCEPTIONS_TAB_ID),
              salesPostingExceptionToAlert.getId(),
              OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_NO_ORDER_FOUND") + ": "
                  + salesPostingExceptionToAlert.getInvoiceReference());
        }
        logger.logln(salesPostingExceptionToAlert.getInvoiceReference() + strNotFound);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static List<SalesPostingExceptions> getSalesPostingExceptionsToAlert() {
    List<SalesPostingExceptions> salesPostingExceptionsToAlert = new ArrayList<SalesPostingExceptions>();
    OBCriteria<SalesPostingExceptions> obCriteria = OBDal.getInstance().createCriteria(
        SalesPostingExceptions.class);
    obCriteria.add(Restrictions.eq(SalesPostingExceptions.PROPERTY_ACTIVE, true));
    obCriteria.add(Restrictions.eq(SalesPostingExceptions.PROPERTY_PROCESSED, false));
    obCriteria.add(Restrictions.eq(SalesPostingExceptions.PROPERTY_ALERTED, false));
    salesPostingExceptionsToAlert = obCriteria.list();
    return salesPostingExceptionsToAlert;
  }

  public static void createSendAlert(Order order, String msg, String description2, String tabId2,
      String recordId2) {
    if (order != null && StringUtils.isNotEmpty(msg)) {
      AlertUtils.createSendAlert(order.getClient(), order.getOrganization(),
          AlertUtils.SALES_POSTING_EXCEPTIONS_ALERT_RULE_ID, msg, order.getIdentifier(),
          order.getId(), description2, tabId2, recordId2);
    }
  }

}