/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.ad.access.OrderLineTax;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.OrderLineOffer;
import org.openbravo.model.common.order.OrderTax;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.retail.posterminal.TerminalType;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.Charges;
import com.openbravo.sharaf.integration.erpob.CustomerDetails;
import com.openbravo.sharaf.integration.erpob.DeliveryInstruction;
import com.openbravo.sharaf.integration.erpob.DeliveryInstructionLines;
import com.openbravo.sharaf.integration.erpob.InvoiceHeader;
import com.openbravo.sharaf.integration.erpob.InvoiceLine;
import com.openbravo.sharaf.integration.erpob.ItemAdditionalInfo;
import com.openbravo.sharaf.integration.erpob.OfferOut;
import com.openbravo.sharaf.integration.erpob.Settlement;
import com.openbravo.sharaf.integration.erpob.edl.ExportSalesDataItemProcessor;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;
import com.openbravo.sharaf.retail.customdevelopments.CUSRSHA_LineCharge;

public class ExportInvoice extends DalBaseProcess {

  private static ProcessLogger logger;
  private static final Logger log = Logger.getLogger(ExportInvoice.class);

  public static final String INVOICE_EDL_PROCESS_ID = "A6EFEA94415340C792B450FC43519704";
  public static final String TABLE_NAME = "Order";
  public static final String PRICE_ADJUSTMENT_PROMO_TYPE_ID = "CA5491E6000647BD889B8D7CDF680795";
  public static final String PROMOTION_PROMO_TYPE_ID = "6A3C2313136147A6B2D1B8D2F5F768E6";
  public static final String PROMOTION_USER_DEFINED_AMOUNT_TYPE_ID = "D1D193305A6443B09B299259493B272A";
  static OBEDLRequest edlRequest = null;
  static Long scannedSerialNo = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of c_order records to be processed
      int recordsToProcess = ExportSalesDataItemProcessor
          .updateOrderRecordsProcessRunToCurrentProcessRun(conn, clientId, adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor.addRequest(INVOICE_EDL_PROCESS_ID,
            ExportSalesDataItemProcessor.getRecords(TABLE_NAME, clientId, adProcessRunId));
        logger.log(edlRequest.getResponse());
      }
    } catch (Exception e) {
      logger.log(e.getMessage());
      // rollback "ad_process_run" column to null of c_order records to be processed
      ExportSalesDataItemProcessor.updateOrderRecordsProcessRunToNull(conn, clientId,
          adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createInvoice(String item) {
    String msg = "";
    Order order = OBDal.getInstance().get(Order.class, item);
    if (order != null) {
      boolean error = false;
      boolean deleteInvoiceHeader = false;
      InvoiceHeader invoiceHeader = null;
      scannedSerialNo = new Long(0);
      try {
        // Create Invoice Header
        invoiceHeader = createInvoiceHeader(order);
        if (invoiceHeader != null) {
          // Create Invoice Lines
          InvoiceLine invoiceLine = createInvoiceLines(invoiceHeader, order);
          // Create Charges
          Charges chargeLine = createChargeLines(invoiceHeader, order);
          if (invoiceLine == null && chargeLine == null) {
            if (invoiceLine == null) {
              msg = String.format(OBMessageUtils.messageBD("OBSHINT_ERROR_CREATING_INVOICE_LINE"),
                  order.getIdentifier());
            }
            if (chargeLine == null) {
              msg = String.format(OBMessageUtils.messageBD("OBSHINT_ERROR_CREATING_CHARGE_LINE"),
                  order.getIdentifier());
            }
            error = true;
            deleteInvoiceHeader = true;
          } else {
            // Create Customer Details
            CustomerDetails customerDetails = createCustomerDetails(invoiceHeader, order);
            if (customerDetails != null) {
              // Create Promotion Out
              createOfferOut(invoiceHeader, order);
              // Create Additional Information
              createItemAdditionalInfo(invoiceHeader, order);
              // Create Settlements
              createSettlement(invoiceHeader, order);
              // Create Delivery Instruction
              DeliveryInstruction deliveryInstruction = createDeliveryInstruction(invoiceHeader,
                  order);
              if (deliveryInstruction != null) {
                // Create Delivery Instruction Lines
                DeliveryInstructionLines deliveryInstructionLines = createDeliveryInstructionLines(
                    deliveryInstruction, order);
                if (deliveryInstructionLines == null) {
                  OBDal.getInstance().remove(deliveryInstruction);
                }
              }
              // Set Sales Order as exported
              order.setObshintIsexported(true);
            } else {
              msg = String.format(
                  OBMessageUtils.messageBD("OBSHINT_ERROR_CREATING_CUSTOMER_DETAILS"),
                  order.getIdentifier());
              error = true;
              deleteInvoiceHeader = true;
            }
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_ERROR_CREATING_INVOICE_HEADER"),
              order.getIdentifier());
          error = true;
        }
      } catch (Exception e) {
        logger.logln("Error exporting sales order: " + e.getMessage());
        log.error("Error exporting sales order", e);
        msg = String.format(OBMessageUtils.messageBD("OBSHINT_ERROR_EXPORTING_SALES_ORDER"),
            order.getIdentifier() + ": " + e.getMessage());
        error = true;
        deleteInvoiceHeader = true;
      }
      if (deleteInvoiceHeader && invoiceHeader != null) {
        OBDal.getInstance().remove(invoiceHeader);
      }
      order.setObshintProcessrun(null);
      if (error) {
        try {
          OBDal.getInstance().getConnection().commit();
        } catch (SQLException e) {
          throw new OBException(e);
        }
        throw new OBException(msg);
      }
    }
  }

  public static BigDecimal calculateItemsTotal(Order order) {
    BigDecimal chargesTotal = BigDecimal.ZERO;
    if (order != null) {
      for (OrderLine orderLine : order.getOrderLineList()) {
        if (orderLine != null && orderLine.getProduct() != null
            && "I".equals(orderLine.getProduct().getProductType()) && !orderLine.isObposIsDeleted()) {
          chargesTotal = chargesTotal.add(orderLine.getLineGrossAmount());
        }
      }
    }
    return chargesTotal;
  }

  public static BigDecimal calculateChargesTotal(Order order) {
    BigDecimal chargesTotal = BigDecimal.ZERO;
    if (order != null) {
      for (OrderLine orderLine : order.getOrderLineList()) {
        if (orderLine != null && orderLine.getProduct() != null && !orderLine.isObposIsDeleted()
            && "S".equals(orderLine.getProduct().getProductType())) {
          // Add Services Lines amount
          chargesTotal = chargesTotal.add(orderLine.getLineGrossAmount());
        }
      }
      // Add Services + Items tax lines amount
      chargesTotal = chargesTotal.add(calculateTotalTaxOrder(order));
    }
    return chargesTotal;
  }

  public static BigDecimal calculateTotalTaxOrderLine(OrderLine orderLine) {
    BigDecimal totalTaxOrderLine = BigDecimal.ZERO;
    if (orderLine != null && !orderLine.isObposIsDeleted()) {
      for (OrderLineTax orderLineTax : orderLine.getOrderLineTaxList()) {
        totalTaxOrderLine = totalTaxOrderLine.add(orderLineTax.getTaxAmount());
      }
    }
    return totalTaxOrderLine;
  }

  public static BigDecimal calculateTotalTaxOrder(Order order) {
    BigDecimal totalTaxOrder = BigDecimal.ZERO;
    if (order != null) {
      for (OrderTax orderTax : order.getOrderTaxList()) {
        totalTaxOrder = totalTaxOrder.add(orderTax.getTaxAmount());
      }
    }
    return totalTaxOrder;
  }

  public static BigDecimal calculateTotalSharafPriceAdjustmentDiscountOrderLine(OrderLine orderLine) {
    BigDecimal totalDiscountOrderLine = BigDecimal.ZERO;
    if (orderLine != null) {
      for (OrderLineOffer orderLineOffer : orderLine.getOrderLineOfferList()) {
        if (orderLineOffer.getPriceAdjustment().getDiscountType().getId()
            .equals(PRICE_ADJUSTMENT_PROMO_TYPE_ID)
            || orderLineOffer.getPriceAdjustment().getDiscountType().getId()
                .equals(PROMOTION_PROMO_TYPE_ID)) {
          totalDiscountOrderLine = totalDiscountOrderLine.add(orderLineOffer
              .getPriceAdjustmentAmt());
        }
      }
    }
    return totalDiscountOrderLine;
  }

  public static BigDecimal calculateTotalUserDefinedAmountDiscountOrderLine(OrderLine orderLine) {
    BigDecimal totalDiscountOrderLine = BigDecimal.ZERO;
    if (orderLine != null) {
      for (OrderLineOffer orderLineOffer : orderLine.getOrderLineOfferList()) {
        if (orderLineOffer.getPriceAdjustment().getDiscountType().getId()
            .equals(PROMOTION_USER_DEFINED_AMOUNT_TYPE_ID)) {
          totalDiscountOrderLine = totalDiscountOrderLine.add(orderLineOffer.getTotalAmount());
        }
      }
    }
    return totalDiscountOrderLine;
  }

  public static BigDecimal calculateTotalDiscountOrder(Order order) {
    BigDecimal totalDiscountOrder = BigDecimal.ZERO;
    if (order != null) {
      for (OrderLine orderLine : order.getOrderLineList()) {
        for (OrderLineOffer orderLineOffer : orderLine.getOrderLineOfferList()) {
          totalDiscountOrder = totalDiscountOrder.add(orderLineOffer.getTotalAmount());
        }
      }
    }
    return totalDiscountOrder;
  }

  public static BigDecimal calculateAdvanceCollected(Order order) {
    BigDecimal advanceCollected = BigDecimal.ZERO;
    if (order != null
        && order.getCustsdtDocumenttype() != null
        && ("AR".equals(order.getCustsdtDocumenttype().getSearchKey()) || "AD".equals(order
            .getCustsdtDocumenttype().getSearchKey()))) {
      advanceCollected = calculatePaid(order);
    }
    return advanceCollected;
  }

  public static String getSettlementCode(Order order, FIN_PaymentDetail paymentDetail) {
    TerminalType terminalType = order.getObposApplications().getObposTerminaltype();
    FIN_Payment payment = paymentDetail.getFinPayment();

    OBCriteria<TerminalTypePaymentMethod> obcPaymentMethod = OBDal.getInstance().createCriteria(
        TerminalTypePaymentMethod.class);
    obcPaymentMethod.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_OBPOSTERMINALTYPE,
        terminalType));
    obcPaymentMethod.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_PAYMENTMETHOD,
        payment.getPaymentMethod()));
    obcPaymentMethod.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_CURRENCY, payment
        .getAccount().getCurrency()));
    obcPaymentMethod.setMaxResults(1);
    if (obcPaymentMethod.list().size() > 0) {
      return obcPaymentMethod.list().get(0).getSearchKey();
    }
    return null;
  }

  public static List<FIN_PaymentDetail> getPaymentDetails(Order order) {
    List<FIN_PaymentDetail> paymentDetails = new ArrayList<FIN_PaymentDetail>();
    if (order != null) {
      for (FIN_PaymentSchedule orderPaymentSchedule : order.getFINPaymentScheduleList()) {
        for (FIN_PaymentScheduleDetail orderPaymentScheduleDetail : orderPaymentSchedule
            .getFINPaymentScheduleDetailOrderPaymentScheduleList()) {
          if (orderPaymentScheduleDetail.getPaymentDetails() != null) {
            paymentDetails.add(orderPaymentScheduleDetail.getPaymentDetails());
          }
        }
      }
    }
    return paymentDetails;
  }

  public static BigDecimal calculatePaid(Order order) {
    BigDecimal paid = BigDecimal.ZERO;
    if (order != null) {
      List<FIN_PaymentDetail> paymentDetails = getPaymentDetails(order);
      for (FIN_PaymentDetail paymentDetail : paymentDetails) {
        paid = paid.add(paymentDetail.getAmount());
      }
    }
    return paid;
  }

  public static Order getOrderByInvoiceReference(String invoiceReference) {
    Order salesOrder = null;
    if (StringUtils.isNotEmpty(invoiceReference)) {
      OBCriteria<Order> obCriteria = OBDal.getInstance().createCriteria(Order.class);
      obCriteria.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
      obCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, invoiceReference));
      List<Order> salesOrders = obCriteria.list();
      if (!salesOrders.isEmpty()) {
        salesOrder = salesOrders.get(0);
      }
    }
    return salesOrder;
  }

  public static InvoiceHeader createInvoiceHeader(Order order) {
    InvoiceHeader invoiceHeader = null;
    if (order != null) {
      if (order.getCustsdtDocumenttype() == null || order.isObposIsDeleted()) {
        return null;
      }
      if (order.getCreatedBy().getBusinessPartner() == null) {
        return null;
      }
      boolean isReturn = order.getDocumentType().isReturn();
      invoiceHeader = OBProvider.getInstance().get(InvoiceHeader.class);
      invoiceHeader.setClient(order.getClient());
      invoiceHeader.setClientSharaf(order.getClient().getId());
      invoiceHeader.setOrganization(order.getOrganization());
      invoiceHeader.setOrganizationSharaf(order.getOrganization().getSearchKey());
      invoiceHeader.setActive(order.isActive());
      invoiceHeader.setInvoiceReference(order.getDocumentNo());
      invoiceHeader.setInvoiceDate(order.getPOSSBusinessDate() != null ? order
          .getPOSSBusinessDate() : order.getOrderDate());
      invoiceHeader.setTransactionType(order.getCustsdtDocumenttype().getSearchKey());
      invoiceHeader.setBillingStoreCode(order.getOrganization().getSearchKey());
      invoiceHeader.setTransStartDateTime(order.getObposCreatedabsolute());
      invoiceHeader.setTransEndDateTime(order.getObposCreatedabsolute());
      invoiceHeader.setTransExpiryDate(null);
      invoiceHeader.setTillCode(order.getObposApplications() != null ? order.getObposApplications()
          .getSearchKey() : null);
      invoiceHeader.setCashierCode(order.getCreatedBy().getBusinessPartner().getSearchKey());
      invoiceHeader.setCustomerCode(order.getBusinessPartner().getSearchKey());
      invoiceHeader.setCurrencyCode(order.getCurrency().getISOCode());
      BigDecimal total = order.getGrandTotalAmount();
      invoiceHeader.setHeaderDiscountAmount(BigDecimal.ZERO);
      invoiceHeader.setHeaderDiscountPercentage(BigDecimal.ZERO);
      invoiceHeader.setHeaderValueAfterDiscount(isReturn ? total.negate() : total);
      invoiceHeader.setInvoiceItemTotal(isReturn ? calculateItemsTotal(order).negate()
          : calculateItemsTotal(order));
      BigDecimal advanceCollected = calculateAdvanceCollected(order);
      invoiceHeader.setAdvanceCollected(isReturn ? advanceCollected.negate() : advanceCollected);
      invoiceHeader.setChargesTotal(isReturn ? calculateChargesTotal(order).negate()
          : calculateChargesTotal(order));
      invoiceHeader.setRoundoffAmount(BigDecimal.ZERO);
      invoiceHeader.setTransTotalLocalAmt(isReturn ? total.negate() : total);
      invoiceHeader.setAirmilePoints(isReturn ? order.getCustarmEarnedpoints() * -1 : order
          .getCustarmEarnedpoints());
      OBDal.getInstance().save(invoiceHeader);
    }
    return invoiceHeader;
  }

  public static InvoiceLine createInvoiceLines(InvoiceHeader invoiceHeader, Order order) {
    InvoiceLine invoiceLine = null;
    if (invoiceHeader != null && order != null) {
      boolean isReturn = order.getDocumentType().isReturn();
      for (OrderLine orderLine : order.getOrderLineList()) {
        // Regular product lines
        if (orderLine != null && orderLine.getProduct() != null
            && !"S".equals(orderLine.getProduct().getProductType())
            && !orderLine.isObposIsDeleted()) {
          invoiceLine = OBProvider.getInstance().get(InvoiceLine.class);
          invoiceLine.setObshintInvoice(invoiceHeader);
          invoiceLine.setClient(invoiceHeader.getClient());
          invoiceLine.setClientSharaf(invoiceHeader.getClient().getId());
          invoiceLine.setOrganization(invoiceHeader.getOrganization());
          invoiceLine.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
          invoiceLine.setActive(invoiceHeader.isActive());
          invoiceLine.setInvoiceReference(invoiceHeader.getInvoiceReference());
          invoiceLine.setItemCode(orderLine.getProduct().getSearchKey());
          invoiceLine.setUOMCode(orderLine.getUOM() != null ? orderLine.getUOM().getName() : null);
          String stockType = "N"; // Normal
          if (orderLine.getCUSTSHAStockType() != null) {
            switch (orderLine.getCUSTSHAStockType()) {
            case "Display":
              stockType = "B";
              break;
            case "Gift":
              stockType = "F";
              break;
            }
          }
          invoiceLine.setStockTypeCode(stockType);
          invoiceLine.setItemQuantity(isReturn ? orderLine.getOrderedQuantity().negate()
              : orderLine.getOrderedQuantity());
          // TODO: To be clarified if taxes has to be included or not. To also be clarified if Price
          // List is "Including taxes". Implemented using "PriceList including taxes".
          // Discounted Amount in "Sharaf Price Adjustment" Promotion type
          BigDecimal totalSharafUnitPriceAdjustmentDiscountOrderLine = calculateTotalSharafPriceAdjustmentDiscountOrderLine(orderLine);
          // Discounted Amount in "User Defined Amount" Promotion type
          BigDecimal totalManuaDiscountOrderLine = calculateTotalUserDefinedAmountDiscountOrderLine(
              orderLine).abs();
          // Net Amount = Amount after all discounts ("Sharaf Promotions" and "Manual Discounts")
          BigDecimal netAmount = orderLine.getLineGrossAmount();
          // Gross Amount = RSP - "Sharaf Promotions"
          BigDecimal grossUnitAmount = orderLine.getGrossListPrice().subtract(
              totalSharafUnitPriceAdjustmentDiscountOrderLine);
          BigDecimal grossAmount = grossUnitAmount.multiply(orderLine.getOrderedQuantity().abs());
          // Percentage of "User Defined Amount" promotion type
          BigDecimal percentageDiscountOrderLine = BigDecimal.ZERO;
          if (grossAmount.compareTo(BigDecimal.ZERO) != 0) {
            percentageDiscountOrderLine = totalManuaDiscountOrderLine.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO
                : totalManuaDiscountOrderLine.divide(grossAmount, 2, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(100));
          }
          invoiceLine.setSellingPrice(grossUnitAmount);
          invoiceLine.setItemGrossAmount(grossAmount);
          invoiceLine.setItemNetAmount(isReturn ? netAmount.negate() : netAmount);
          invoiceLine.setItemDiscountAmount(totalManuaDiscountOrderLine);
          invoiceLine.setItemDiscountPercent(percentageDiscountOrderLine);
          invoiceLine.setItemDiscountHeaderAmount(BigDecimal.ZERO);
          BigDecimal totalTaxOrderLine = calculateTotalTaxOrderLine(orderLine);
          BigDecimal percentageTaxOrderLine = orderLine.getTax().getRate();
          invoiceLine.setItemTaxAmount(isReturn ? totalTaxOrderLine.negate() : totalTaxOrderLine);
          invoiceLine.setItemTaxPercentage(percentageTaxOrderLine);
          invoiceLine.setSalesmanCode(orderLine.getShasrSalesrep() != null ? orderLine
              .getShasrSalesrep().getBusinessPartner().getSearchKey() : invoiceHeader
              .getCashierCode());
          invoiceLine.setScannedBarcode(null);
          invoiceLine.setScannedSerialNumber(orderLine.getLineNo() / 10);
          invoiceLine.setOriginalSellingPrice(orderLine.getGrossListPrice());
          invoiceLine.setAirmilePoints(isReturn ? orderLine.getCustarmEarnedpoints() * -1
              : orderLine.getCustarmEarnedpoints());
          OBDal.getInstance().save(invoiceLine);

          createChargeTaxLine(invoiceHeader, order, orderLine, null, totalTaxOrderLine);
        }
      }
    }
    return invoiceLine;
  }

  public static Charges createChargeLines(InvoiceHeader invoiceHeader, Order order) {
    Charges chargeLine = null;
    if (invoiceHeader != null && order != null) {
      BigDecimal itemsAmountInSalesOrder = itemsAmountInSalesOrder(order);
      for (OrderLine orderLine : order.getOrderLineList()) {
        // Service product lines
        if (orderLine != null && orderLine.getProduct() != null
            && "S".equals(orderLine.getProduct().getProductType()) && !orderLine.isObposIsDeleted()) {
          // Line Gross Amount
          BigDecimal lineGrossAmount = orderLine.getLineGrossAmount();
          // Line manual discounts
          BigDecimal totalManuaDiscountOrderLine = calculateTotalUserDefinedAmountDiscountOrderLine(orderLine);
          // Line Air Mile Points
          BigDecimal lineAirmilePoints = BigDecimal.valueOf(orderLine.getCustarmEarnedpoints());
          // Line Tax Amount
          BigDecimal lineTaxAmount = calculateTotalTaxOrderLine(orderLine);
          // Service Linked to Products
          if (orderLine.getOrderlineServiceRelationList().size() > 0) {
            // It should only be one related item as the services are configured as "grouped" and
            // "As per Product".
            OrderLine relatedItemLine = orderLine.getOrderlineServiceRelationList().get(0)
                .getOrderlineRelated();
            chargeLine = createChargeLine(invoiceHeader, orderLine, relatedItemLine,
                lineGrossAmount, totalManuaDiscountOrderLine, lineAirmilePoints);
            // Create Tax Charge line for Related Item Line
            createChargeTaxLine(invoiceHeader, order, orderLine, relatedItemLine, lineTaxAmount);
            // Service Linked to Blind Return Lines
          } else if (orderLine.getCustshaLineChargeList().size() > 0) {
            // The service could be related to multiple blind return lines.
            // Line Gross Amount
            BigDecimal pendingAmountToBeDistributed = lineGrossAmount;
            BigDecimal lineAmount = BigDecimal.ZERO;
            // Line manual discounts
            BigDecimal pendingTotalManuaDiscountOrderLineToBeDistributed = totalManuaDiscountOrderLine;
            BigDecimal lineManualDiscountsAmount = BigDecimal.ZERO;
            // Line Air Mile Points
            BigDecimal pendingAirmilePointsToBeDistributed = lineAirmilePoints;
            BigDecimal lineAirMilePointsQty = BigDecimal.ZERO;
            // Line Tax Amount
            BigDecimal pendingTaxAmountToBeDistributed = lineTaxAmount;
            BigDecimal chargeLineTaxAmount = BigDecimal.ZERO;

            BigDecimal itemsTotalAmount = BigDecimal.ZERO;
            BigDecimal rate = BigDecimal.ZERO;

            int relatedLinesSize = orderLine.getCustshaLineChargeList().size();
            int i = 0;
            // Get total amount of related blind return lines
            for (CUSRSHA_LineCharge lineCharge : orderLine.getCustshaLineChargeList()) {
              itemsTotalAmount = itemsTotalAmount.add(lineCharge.getChargeOrderline()
                  .getLineGrossAmount());
            }
            for (CUSRSHA_LineCharge lineCharge : orderLine.getCustshaLineChargeList()) {
              // Get the percentage over the total blind return lines of the current blind return
              // line
              // If is the last iteration assign pending amount to avoid rounding issues
              if (i == relatedLinesSize - 1) {
                lineAmount = pendingAmountToBeDistributed;
                lineManualDiscountsAmount = pendingTotalManuaDiscountOrderLineToBeDistributed;
                lineAirMilePointsQty = pendingAirmilePointsToBeDistributed;
                chargeLineTaxAmount = pendingTaxAmountToBeDistributed;
              } else {
                rate = lineCharge.getChargeOrderline().getLineGrossAmount()
                    .divide(itemsTotalAmount, 32, RoundingMode.HALF_UP);
                lineAmount = lineGrossAmount.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                lineManualDiscountsAmount = totalManuaDiscountOrderLine.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                lineAirMilePointsQty = lineAirmilePoints.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                chargeLineTaxAmount = lineTaxAmount.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
              }
              chargeLine = createChargeLine(invoiceHeader, orderLine,
                  lineCharge.getChargeOrderline(), lineAmount, lineManualDiscountsAmount,
                  lineAirMilePointsQty);
              pendingAmountToBeDistributed = pendingAmountToBeDistributed.subtract(lineAmount);
              pendingTotalManuaDiscountOrderLineToBeDistributed = pendingTotalManuaDiscountOrderLineToBeDistributed
                  .subtract(lineManualDiscountsAmount);
              pendingAirmilePointsToBeDistributed = pendingAirmilePointsToBeDistributed
                  .subtract(lineAirMilePointsQty);
              pendingTaxAmountToBeDistributed = pendingTaxAmountToBeDistributed
                  .subtract(chargeLineTaxAmount);
              createChargeTaxLine(invoiceHeader, order, orderLine, lineCharge.getChargeOrderline(),
                  chargeLineTaxAmount);
              i++;
            }
            // Global Services not Linked to any other line, but other item lines exist.
          } else if (itemsAmountInSalesOrder.compareTo(BigDecimal.ZERO) != 0) {
            // The service has to be related to all items in the sales order.
            // Line Gross Amount
            BigDecimal pendingAmountToBeDistributed = lineGrossAmount;
            BigDecimal lineAmount = BigDecimal.ZERO;
            // Line manual discounts
            BigDecimal pendingTotalManuaDiscountOrderLineToBeDistributed = totalManuaDiscountOrderLine;
            BigDecimal lineManualDiscountsAmount = BigDecimal.ZERO;
            // Line Air Mile Points
            BigDecimal pendingAirmilePointsToBeDistributed = lineAirmilePoints;
            BigDecimal lineAirMilePointsQty = BigDecimal.ZERO;
            // Line Tax Amount
            BigDecimal pendingTaxAmountToBeDistributed = lineTaxAmount;
            BigDecimal chargeLineTaxAmount = BigDecimal.ZERO;

            BigDecimal rate = BigDecimal.ZERO;

            List<String> orderLineList = new ArrayList<String>();
            for (OrderLine line : order.getOrderLineList()) {
              // Exclude deleted lines and Services
              if (line.isObposIsDeleted() || !"I".equals(line.getProduct().getProductType())) {
                continue;
              }
              orderLineList.add(line.getId());
            }
            int relatedLinesSize = orderLineList.size();
            int i = 0;
            for (String lineId : orderLineList) {
              OrderLine line = OBDal.getInstance().get(OrderLine.class, lineId);
              // Get the percentage over the total item lines
              // If is the last iteration assign pending amount to avoid rounding issues
              if (i == relatedLinesSize - 1) {
                lineAmount = pendingAmountToBeDistributed;
                lineManualDiscountsAmount = pendingTotalManuaDiscountOrderLineToBeDistributed;
                lineAirMilePointsQty = pendingAirmilePointsToBeDistributed;
                chargeLineTaxAmount = pendingTaxAmountToBeDistributed;
              } else {
                rate = line.getLineGrossAmount().divide(itemsAmountInSalesOrder, 32,
                    RoundingMode.HALF_UP);
                lineAmount = lineGrossAmount.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                lineManualDiscountsAmount = totalManuaDiscountOrderLine.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                lineAirMilePointsQty = lineAirmilePoints.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
                chargeLineTaxAmount = lineTaxAmount.multiply(rate)
                    .setScale(orderLine.getCurrency().getStandardPrecision().intValue(),
                        RoundingMode.HALF_UP);
              }
              chargeLine = createChargeLine(invoiceHeader, orderLine, line, lineAmount,
                  lineManualDiscountsAmount, lineAirMilePointsQty);
              pendingAmountToBeDistributed = pendingAmountToBeDistributed.subtract(lineAmount);
              pendingTotalManuaDiscountOrderLineToBeDistributed = pendingTotalManuaDiscountOrderLineToBeDistributed
                  .subtract(lineManualDiscountsAmount);
              pendingAirmilePointsToBeDistributed = pendingAirmilePointsToBeDistributed
                  .subtract(lineAirMilePointsQty);
              pendingTaxAmountToBeDistributed = pendingTaxAmountToBeDistributed
                  .subtract(chargeLineTaxAmount);
              createChargeTaxLine(invoiceHeader, order, orderLine, line, chargeLineTaxAmount);
              i++;
            }
            // Global Services not Linked to any other line, but there are no any other item lines.
          } else {
            chargeLine = createChargeLine(invoiceHeader, orderLine, null, lineGrossAmount,
                totalManuaDiscountOrderLine, lineAirmilePoints);
            createChargeTaxLine(invoiceHeader, order, orderLine, null, lineTaxAmount);
          }
        }
      }
    }
    return chargeLine;
  }

  public static BigDecimal itemsAmountInSalesOrder(Order order) {
    BigDecimal itemsAmountInSalesOrder = BigDecimal.ZERO;
    for (OrderLine orderLine : order.getOrderLineList()) {
      if ("I".equals(orderLine.getProduct().getProductType()) && !orderLine.isObposIsDeleted()) {
        itemsAmountInSalesOrder = itemsAmountInSalesOrder.add(orderLine.getLineGrossAmount());
      }
    }
    return itemsAmountInSalesOrder;
  }

  public static Charges createChargeLine(InvoiceHeader invoiceHeader, OrderLine orderLine,
      OrderLine relatedItemLine, BigDecimal netAmount, BigDecimal totalManuaDiscountOrderLine,
      BigDecimal airMilePoints) {
    boolean isReturn = orderLine.getSalesOrder().getDocumentType().isReturn();
    Charges chargeLine = OBProvider.getInstance().get(Charges.class);
    chargeLine.setObshintInvoice(invoiceHeader);
    chargeLine.setClient(invoiceHeader.getClient());
    chargeLine.setClientSharaf(invoiceHeader.getClient().getId());
    chargeLine.setOrganization(invoiceHeader.getOrganization());
    chargeLine.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
    chargeLine.setActive(invoiceHeader.isActive());
    chargeLine.setInvoiceReference(invoiceHeader.getInvoiceReference());
    chargeLine.setChargeCode(orderLine.getProduct().getSearchKey());
    chargeLine.setSalesmanCode(orderLine.getShasrSalesrep() != null ? orderLine.getShasrSalesrep()
        .getBusinessPartner().getSearchKey() : invoiceHeader.getCashierCode());
    chargeLine.setChargeQuantity(isReturn ? orderLine.getOrderedQuantity().negate() : orderLine
        .getOrderedQuantity());
    // Discounted Amount in "Sharaf Price Adjustment" Promotion type
    // There are no Promotions for Service Type products
    // Discounted Amount in "User Defined Amount" Promotion type
    // Net Amount = Amount after all discounts ("Sharaf Promotions" and "Manual Discounts")
    // Gross Amount = RSP - "Sharaf Promotions"
    // There are no Promotions for Service Type products
    BigDecimal grossAmount = netAmount;
    BigDecimal unitGrossAmount = grossAmount.divide(orderLine.getOrderedQuantity(), 2,
        BigDecimal.ROUND_HALF_UP);
    // Percentage of "User Defined Amount" promotion type
    BigDecimal percentageDiscountOrderLine = BigDecimal.ZERO;
    if (grossAmount.compareTo(BigDecimal.ZERO) != 0) {
      percentageDiscountOrderLine = totalManuaDiscountOrderLine.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO
          : totalManuaDiscountOrderLine.divide(grossAmount, 2, BigDecimal.ROUND_HALF_UP).multiply(
              new BigDecimal(100));
    }
    chargeLine.setChargeSellingPrice(isReturn ? unitGrossAmount.negate() : unitGrossAmount);
    chargeLine.setChargeGrossAmount(isReturn ? grossAmount.negate() : grossAmount);
    chargeLine.setChargeDiscountAmount(isReturn ? totalManuaDiscountOrderLine.negate()
        : totalManuaDiscountOrderLine);
    chargeLine.setChargeDiscountPercentage(percentageDiscountOrderLine);
    chargeLine.setChargeNetAmount(isReturn ? netAmount.negate() : netAmount);
    chargeLine.setChargesSerialNumber(++scannedSerialNo);
    if (relatedItemLine != null) {
      chargeLine.setItemCode(relatedItemLine.getProduct().getSearchKey());
      chargeLine.setScannedSerialNumber(relatedItemLine.getLineNo() / 10);
    }
    chargeLine.setAirmilePoints(isReturn ? airMilePoints.longValue() * -1 : airMilePoints
        .longValue());
    chargeLine.setChargeType("C");
    chargeLine.setApplicableOn(null);
    OBDal.getInstance().save(chargeLine);
    return chargeLine;
  }

  public static void createChargeTaxLine(InvoiceHeader invoiceHeader, Order order,
      OrderLine orderLine, OrderLine relatedItemLine, BigDecimal totalTaxOrderLine) {
    Charges chargeLine = null;
    if (invoiceHeader != null && order != null) {
      boolean isReturn = orderLine.getSalesOrder().getDocumentType().isReturn();
      chargeLine = OBProvider.getInstance().get(Charges.class);
      chargeLine.setObshintInvoice(invoiceHeader);
      chargeLine.setClient(invoiceHeader.getClient());
      chargeLine.setClientSharaf(invoiceHeader.getClient().getId());
      chargeLine.setOrganization(invoiceHeader.getOrganization());
      chargeLine.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
      chargeLine.setActive(invoiceHeader.isActive());
      chargeLine.setInvoiceReference(invoiceHeader.getInvoiceReference());
      chargeLine.setChargeCode(orderLine.getTax().getObshintTaxCode() != null ? orderLine.getTax()
          .getObshintTaxCode() : orderLine.getProduct().getSearchKey());
      chargeLine.setSalesmanCode(orderLine.getShasrSalesrep() != null ? orderLine
          .getShasrSalesrep().getBusinessPartner().getSearchKey() : invoiceHeader.getCashierCode());
      chargeLine.setChargeQuantity(isReturn ? orderLine.getOrderedQuantity().negate() : orderLine
          .getOrderedQuantity());
      chargeLine.setChargeSellingPrice(BigDecimal.ZERO);
      chargeLine.setChargeGrossAmount(BigDecimal.ZERO);
      chargeLine.setChargeDiscountAmount(BigDecimal.ZERO);
      chargeLine.setChargeDiscountPercentage(BigDecimal.ZERO);
      chargeLine.setChargeNetAmount(isReturn ? totalTaxOrderLine.negate() : totalTaxOrderLine);
      chargeLine.setChargesSerialNumber(++scannedSerialNo);
      // Service related to a product
      if (relatedItemLine != null) {
        chargeLine.setItemCode(relatedItemLine.getProduct().getSearchKey());
        chargeLine.setScannedSerialNumber(relatedItemLine.getLineNo() / 10);
        // Product
      } else if (!"S".equals(orderLine.getProduct().getProductType())) {
        chargeLine.setItemCode(orderLine.getProduct().getSearchKey());
        chargeLine.setScannedSerialNumber(orderLine.getLineNo() / 10);
        // Service not related to product
      } else {
        chargeLine.setItemCode(null);
        chargeLine.setScannedSerialNumber(null);
      }
      chargeLine.setAirmilePoints(null);
      chargeLine.setChargeType("T");
      chargeLine.setApplicableOn("S".equals(orderLine.getProduct().getProductType()) ? orderLine
          .getProduct().getSearchKey() : null);

      OBDal.getInstance().save(chargeLine);
    }
  }

  public static void createOfferOut(InvoiceHeader invoiceHeader, Order order) {
    OfferOut offerOut = null;
    for (OrderLine orderLine : order.getOrderLineList()) {
      // Regular product lines
      if (orderLine.getProduct().getProductType().equals("I") && !orderLine.isObposIsDeleted()) {
        offerOut = OBProvider.getInstance().get(OfferOut.class);
        offerOut.setObshintInvoice(invoiceHeader);
        offerOut.setInvoiceRef(order.getDocumentNo());
        offerOut.setItemCode(orderLine.getProduct().getSearchKey());
        offerOut.setScannedItemNo(orderLine.getLineNo() / 10);
        if (orderLine.getCustdisOffer() != null) {
          offerOut.setSharafPromotionID(orderLine.getCustdisOffer().getName());
          if (orderLine.getCustdisOrderline() != null) { // if secondary item
            offerOut.setPromoPrimaryItem(null);
            offerOut.setPromoSecondaryItem(orderLine.getCustdisOffer().getObshintPrimarylink());
            offerOut.setPromoItemFlag("S");
          } else { // if primary item
            offerOut.setPromoPrimaryItem(orderLine.getCustdisOffer().getObshintPrimarylink());
            offerOut.setPromoSecondaryItem(null);
            offerOut.setPromoItemFlag("P");
          }
        }
        offerOut.setLoyaltyCardNo(orderLine.getSalesOrder().getCustarmCardnumber());
        offerOut.setLoyaltyPoints(orderLine.getCustarmEarnedpoints());
        OBDal.getInstance().save(offerOut);
      }
    }
  }

  public static Settlement createSettlement(InvoiceHeader invoiceHeader, Order order) {
    Settlement settlement = null;
    if (invoiceHeader != null && order != null) {
      boolean isReturn = order.getDocumentType().isReturn();
      int settlementSerialNumber = 0;
      List<FIN_PaymentDetail> paymentDetails = getPaymentDetails(order);
      for (FIN_PaymentDetail paymentDetail : paymentDetails) {
        String settlementCode = getSettlementCode(order, paymentDetail);
        if (StringUtils.isEmpty(settlementCode)) {
          return null;
        }
        FIN_Payment payment = paymentDetail.getFinPayment();
        settlement = OBProvider.getInstance().get(Settlement.class);
        settlement.setObshintInvoice(invoiceHeader);
        settlement.setClient(invoiceHeader.getClient());
        settlement.setClientSharaf(invoiceHeader.getClient().getId());
        settlement.setOrganization(invoiceHeader.getOrganization());
        settlement.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
        settlement.setActive(invoiceHeader.isActive());
        settlement.setInvoiceReference(invoiceHeader.getInvoiceReference());
        settlement.setSettlementCode(settlementCode);
        settlement.setCurrencyCode(payment.getAccount().getCurrency().getISOCode());
        settlement.setExchangeRate(payment.getFinancialTransactionConvertRate());
        settlement.setSettlementQuantity(BigDecimal.ONE);
        BigDecimal foreignAmount = paymentDetail.getAmount().multiply(
            payment.getFinancialTransactionConvertRate().setScale(
                payment.getAccount().getCurrency().getStandardPrecision().intValue(),
                RoundingMode.HALF_UP));
        settlement.setSettlementAmtInForeignCurr(isReturn ? foreignAmount.negate() : foreignAmount);
        settlement.setSettlementAmtInLocalCurr(isReturn ? paymentDetail.getAmount().negate()
            : paymentDetail.getAmount());
        // Credit Card Details
        if (payment.getSharccCardDetails() != null) {
          settlement.setSettlementDetail1(settlementCode);
          settlement.setSettlementDetail2(null);
          settlement.setSettlementDetail3(payment.getSharccCardDetails()); // 123456 ****** 7890
          settlement.setSettlementDetail4(payment.getSharccCardAuthcode()); // Authorization number
          settlement.setSettlementDetail5(null);
          settlement.setSettlementDetail6(null);
          settlement.setSettlementDetail7(null);
          // You Got a Gift Details
        } else if (payment.getCustyggCode() != null) {
          settlement.setSettlementDetail1(payment.getCustyggCode());
          settlement.setSettlementDetail2(payment.getCustyggBrand());
          settlement.setSettlementDetail3(payment.getCustyggRedemptionCode());
          settlement.setSettlementDetail4(payment.getCustyggStaff());
          settlement.setSettlementDetail5(payment.getCustyggStore());
          settlement.setSettlementDetail6(payment.getCustyggDateRedeemed() != null ? OBDateUtils
              .formatDate(payment.getCustyggDateRedeemed()) : null);
          settlement.setSettlementDetail7(payment.getCustyggCardAmount() != null ? String
              .valueOf(payment.getCustyggCardAmount().doubleValue()) : null);
          // Airmiles
        } else if (payment.getCustarmAirmilesvouchref() != null) {
          settlement.setSettlementDetail1(payment.getCustarmAirmilesvouchref());
          settlement.setSettlementDetail2(null);
          settlement.setSettlementDetail3(null);
          settlement.setSettlementDetail4(null);
          settlement.setSettlementDetail5(null);
          settlement.setSettlementDetail6(null);
          settlement.setSettlementDetail7(null);
          // // QwikCilver
        } else if (payment.getCUSTQCApprovalCode() != null) {
          // TODO Masked date
          settlement.setSettlementDetail1(payment.getCUSTQCCardNumber()); // Gift Card No (Masked).
          settlement.setSettlementDetail2(payment.getCUSTQCApprovalCode());
          settlement.setSettlementDetail3(Long.toString(payment.getCUSTQCBatchNumber()));
          settlement.setSettlementDetail4(Long.toString(payment.getCUSTQCTransactionID()));
          // TODO Format Expiration Date
          settlement
              .setSettlementDetail5(OBDateUtils.formatDate(payment.getCUSTQCExpirationDate()));
          settlement.setSettlementDetail6(null);
          settlement.setSettlementDetail7(null);
        } else if (payment.getCustshaChequeNumber() != null) {
          settlement.setSettlementDetail1(payment.getCustshaCustomerName());
          settlement.setSettlementDetail2(payment.getCustshaChequeNumber());
          settlement.setSettlementDetail3(null);
          settlement.setSettlementDetail4(null);
          settlement.setSettlementDetail5(null);
          settlement.setSettlementDetail6(null);
          settlement.setSettlementDetail7(null);
        } else {
          settlement.setSettlementDetail1(null);
          settlement.setSettlementDetail2(null);
          settlement.setSettlementDetail3(null);
          settlement.setSettlementDetail4(null);
          settlement.setSettlementDetail5(null);
          settlement.setSettlementDetail6(null);
          settlement.setSettlementDetail7(null);
        }
        settlement.setSettlementDetail8(null);
        settlement.setSettlementDetail9(null);
        settlement.setSettlementDetail10(null);
        settlement.setSettlementSerialNumber(new Long(++settlementSerialNumber));
        OBDal.getInstance().save(settlement);
      }
    }
    return settlement;
  }

  public static CustomerDetails createCustomerDetails(InvoiceHeader invoiceHeader, Order order) {
    CustomerDetails customerDetails = null;
    if (invoiceHeader != null && order != null) {
      User contact = !order.getBusinessPartner().getADUserList().isEmpty() ? order
          .getBusinessPartner().getADUserList().get(0) : null;
      if (contact == null) {
        return null;
      }
      customerDetails = OBProvider.getInstance().get(CustomerDetails.class);
      customerDetails.setObshintInvoice(invoiceHeader);
      customerDetails.setClient(invoiceHeader.getClient());
      customerDetails.setClientSharaf(invoiceHeader.getClient().getId());
      customerDetails.setOrganization(invoiceHeader.getOrganization());
      customerDetails.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
      customerDetails.setActive(invoiceHeader.isActive());
      customerDetails.setInvoiceReference(invoiceHeader.getInvoiceReference());
      customerDetails.setCustomerCode(order.getBusinessPartner().getSearchKey());
      customerDetails.setCustomerCategory(order.getBusinessPartner().getBusinessPartnerCategory()
          .getSearchKey());
      customerDetails.setCustomerGender(true);
      customerDetails.setCustomerTitle(contact.getGreeting() != null ? contact.getGreeting()
          .getTitle() : null);
      customerDetails.setCustomerName(order.getCustshaCustomerName() != null ? order
          .getCustshaCustomerName() : order.getBusinessPartner().getName());
      customerDetails.setCustomerOfficePhone(contact.getPhone());
      customerDetails.setCustomerResiPhone(contact.getAlternativePhone());
      customerDetails.setCustomerEmail(order.getCustshaCustomerEmail() != null ? order
          .getCustshaCustomerEmail() : contact.getEmail());
      customerDetails.setCustomerMobile(order.getCustshaCustomerPhone() != null ? order
          .getCustshaCustomerPhone() : contact.getPhone());
      Location address = !order.getBusinessPartner().getBusinessPartnerLocationList().isEmpty() ? order
          .getBusinessPartner().getBusinessPartnerLocationList().get(0).getLocationAddress()
          : null;
      customerDetails.setCustomerAddress1(order.getCustshaCustomerAddress() != null ? order
          .getCustshaCustomerAddress() : (address != null ? address.getAddressLine1() : null));
      customerDetails.setCustomerAddress2(order.getCustshaCustomerAddress() != null ? null
          : (address != null ? address.getAddressLine2() : null));
      customerDetails.setCustomerAddress3(order.getCustshaWebsiterefno());
      customerDetails.setCustomerAddress4(null);
      customerDetails.setCustomerAddress5(null);
      customerDetails.setCustomerVatNo(order.getBusinessPartner().getTaxID());
      OBDal.getInstance().save(customerDetails);
    }
    return customerDetails;
  }

  public static ItemAdditionalInfo createItemAdditionalInfo(InvoiceHeader invoiceHeader, Order order) {
    ItemAdditionalInfo itemAdditionalInfo = null;
    if (invoiceHeader != null && order != null) {
      boolean isReturn = order.getDocumentType().isReturn();
      for (OrderLine orderLine : order.getOrderLineList()) {
        if (orderLine != null
            && orderLine.getProduct() != null
            && !"S".equals(orderLine.getProduct().getProductType())
            && !orderLine.isObposIsDeleted()
            && (StringUtils.isNotEmpty(orderLine.getObposEpccode()) || StringUtils
                .isNotEmpty(orderLine.getObposSerialNumber()))) {
          itemAdditionalInfo = OBProvider.getInstance().get(ItemAdditionalInfo.class);
          itemAdditionalInfo.setObshintInvoice(invoiceHeader);
          itemAdditionalInfo.setClient(invoiceHeader.getClient());
          itemAdditionalInfo.setClientSharaf(invoiceHeader.getClient().getId());
          itemAdditionalInfo.setOrganization(invoiceHeader.getOrganization());
          itemAdditionalInfo.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
          itemAdditionalInfo.setActive(invoiceHeader.isActive());
          itemAdditionalInfo.setInvoiceReference(invoiceHeader.getInvoiceReference());
          itemAdditionalInfo.setItemCode(orderLine.getProduct().getSearchKey());
          itemAdditionalInfo.setItemSerialNumber(orderLine.getLineNo() / 10);
          itemAdditionalInfo.setItemQuantity(isReturn ? orderLine.getOrderedQuantity().negate()
              : orderLine.getOrderedQuantity());
          itemAdditionalInfo.setProductSerial(orderLine.getObposSerialNumber());
          itemAdditionalInfo.setProductRfid(orderLine.getObposEpccode());
          itemAdditionalInfo.setProductExtendedWty(null);
          OBDal.getInstance().save(itemAdditionalInfo);
        }
      }
    }
    return itemAdditionalInfo;
  }

  public static DeliveryInstruction createDeliveryInstruction(InvoiceHeader invoiceHeader,
      Order order) {
    DeliveryInstruction deliveryInstruction = null;
    if (invoiceHeader != null && order != null) {
      User contact = !order.getBusinessPartner().getADUserList().isEmpty() ? order
          .getBusinessPartner().getADUserList().get(0) : null;
      if (contact == null) {
        return null;
      }
      if (order.getDocumentType().isReturn()) {
        return null;
      }
      deliveryInstruction = OBProvider.getInstance().get(DeliveryInstruction.class);
      deliveryInstruction.setInvoice(invoiceHeader);
      deliveryInstruction.setClient(invoiceHeader.getClient());
      deliveryInstruction.setClientSharaf(invoiceHeader.getClient().getId());
      deliveryInstruction.setOrganization(invoiceHeader.getOrganization());
      deliveryInstruction.setOrganizationSharaf(invoiceHeader.getOrganization().getSearchKey());
      deliveryInstruction.setActive(invoiceHeader.isActive());
      deliveryInstruction.setInvoiceReference(invoiceHeader.getInvoiceReference());
      deliveryInstruction.setDelivInstrReference("xx");// TODO: mandatory
      deliveryInstruction.setDelivInstrDate(order.getOrderDate());
      deliveryInstruction.setDelivVendorCode("WA001");
      deliveryInstruction.setDelivCustomerCode(null);
      deliveryInstruction.setCustomerTitle(contact.getGreeting() != null ? contact.getGreeting()
          .getTitle() : null);
      deliveryInstruction.setCustomerContactPerson(contact.getName());
      Location address = !order.getBusinessPartner().getBusinessPartnerLocationList().isEmpty() ? order
          .getBusinessPartner().getBusinessPartnerLocationList().get(0).getLocationAddress()
          : null;
      deliveryInstruction.setCustomerAddress(address != null ? address.getAddressLine1() + ", "
          + address.getCountry().getName() : null);
      deliveryInstruction.setCustomerMobileNo(contact.getPhone());
      deliveryInstruction.setCustomerResidencePhone(contact.getAlternativePhone());
      deliveryInstruction.setCustomerOfficePhone(contact.getPhone());
      deliveryInstruction.setDelivDriverName(null);
      deliveryInstruction.setDelivDriverContactNo(null);
      deliveryInstruction.setDelivDriverContactNo(null);
      deliveryInstruction.setDelivDrivingLicenseNo(null);
      deliveryInstruction.setDelivLicenseExpiryDate(null);
      deliveryInstruction.setDelivVehicleType(null);
      deliveryInstruction.setDelivVehicleNumber(null);
      OBDal.getInstance().save(deliveryInstruction);
    }
    return deliveryInstruction;
  }

  public static DeliveryInstructionLines createDeliveryInstructionLines(
      DeliveryInstruction deliveryInstruction, Order order) {
    DeliveryInstructionLines deliveryInstructionLine = null;
    if (deliveryInstruction != null && order != null) {
      for (OrderLine orderLine : order.getOrderLineList()) {
        if (orderLine != null && orderLine.getProduct() != null
            && !"S".equals(orderLine.getProduct().getProductType())
            && !orderLine.isObposIsDeleted()) {
          deliveryInstructionLine = OBProvider.getInstance().get(DeliveryInstructionLines.class);
          deliveryInstructionLine.setObshintDelinstruction(deliveryInstruction);
          deliveryInstructionLine.setClient(deliveryInstruction.getClient());
          deliveryInstructionLine.setClientSharaf(deliveryInstruction.getClient().getId());
          deliveryInstructionLine.setOrganization(deliveryInstruction.getOrganization());
          deliveryInstructionLine.setOrganizationSharaf(deliveryInstruction.getOrganization()
              .getSearchKey());
          deliveryInstructionLine.setActive(deliveryInstruction.isActive());
          deliveryInstructionLine.setDelivInstrReference(deliveryInstruction
              .getDelivInstrReference());
          deliveryInstructionLine.setDelivItemCode(orderLine.getProduct().getSearchKey());
          deliveryInstructionLine.setDelivSerialNumber(orderLine.getLineNo() / 10);
          deliveryInstructionLine.setDelivQuantity(orderLine.getOrderedQuantity());
          deliveryInstructionLine.setDelivPackingInstruction(orderLine.getDescription());
          deliveryInstructionLine.setDelivItemSerialNumber(orderLine.getLineNo() / 10);
          String deliveryType = "A"; // Immediate
          if (orderLine.getCUSTDELDeliveryCondition() != null) {
            switch (orderLine.getCUSTDELDeliveryCondition()) {
            case "CD":
              deliveryType = "B"; // Counter
              break;
            case "WD":
              deliveryType = "C"; // Warehouse
              break;
            case "SD":
              deliveryType = "C"; // Supplier
              break;
            }
          }
          deliveryInstructionLine.setDelivType(deliveryType);
          deliveryInstructionLine.setDelivDateFrom(orderLine.getCUSTDELDeliveryTime());
          deliveryInstructionLine.setDelivDateTo(orderLine.getCUSTDELDeliveryTime());
          deliveryInstructionLine.setDeliveryPreferredTime(new Timestamp(orderLine
              .getCUSTDELDeliveryTime().getTime()));
          OBDal.getInstance().save(deliveryInstructionLine);
        }
      }
    }
    return deliveryInstructionLine;
  }

}
