package com.openbravo.sharaf.integration.erpob.process;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.common.order.Order;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

import com.openbravo.sharaf.integration.erpob.SalesPostingExceptions;
import com.openbravo.sharaf.integration.erpob.utility.AlertUtils;

public class SalesPostingExceptionsProcess extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String SALES_ORDER_TAB_ID = "186";

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();

    try {
      OBContext.setAdminMode(true);
      String msg = OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_PROCESSING_COUNT");
      List<SalesPostingExceptions> salesPostingExceptionsToProcess = getSalesPostingExceptionsToProcess();
      logger.logln(msg + ": " + salesPostingExceptionsToProcess.size());
      for (SalesPostingExceptions salesPostingExceptionToProcess : salesPostingExceptionsToProcess) {
        Order salesOrder = ExportInvoice.getOrderByInvoiceReference(salesPostingExceptionToProcess
            .getInvoiceReference());
        String strNotFound = "";
        if (salesOrder != null) {
          salesOrder.setObshintIsexported(false);
          salesPostingExceptionToProcess.setProcessed(true);
          AlertUtils.createNote(OBDal.getInstance().get(Tab.class, SALES_ORDER_TAB_ID), salesOrder
              .getId(), String.format(
              OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_ORDER_TO_REEXPORT"),
              salesOrder.getIdentifier()));
        } else {
          strNotFound = ": "
              + OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_NO_ORDER_FOUND");
          AlertUtils.createNote(
              OBDal.getInstance().get(Tab.class,
                  SalesPostingExceptionsAlert.SALES_POSTING_EXCEPTIONS_TAB_ID),
              salesPostingExceptionToProcess.getId(),
              OBMessageUtils.messageBD("OBSHINT_SALES_POSTING_EXCEPTIONS_NO_ORDER_FOUND") + ": "
                  + salesPostingExceptionToProcess.getInvoiceReference());
        }
        logger.logln(salesPostingExceptionToProcess.getInvoiceReference() + strNotFound);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static List<SalesPostingExceptions> getSalesPostingExceptionsToProcess() {
    List<SalesPostingExceptions> salesPostingExceptionsToProcess = new ArrayList<SalesPostingExceptions>();
    OBCriteria<SalesPostingExceptions> obCriteria = OBDal.getInstance().createCriteria(
        SalesPostingExceptions.class);
    obCriteria.add(Restrictions.eq(SalesPostingExceptions.PROPERTY_ACTIVE, true));
    obCriteria.add(Restrictions.eq(SalesPostingExceptions.PROPERTY_PROCESSED, false));
    obCriteria.add(Restrictions.isNotNull(SalesPostingExceptions.PROPERTY_ERRORFIXEDDATE));
    salesPostingExceptionsToProcess = obCriteria.list();
    return salesPostingExceptionsToProcess;
  }

}