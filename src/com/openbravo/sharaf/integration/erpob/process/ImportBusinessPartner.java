/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.BusinessPartner;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportBusinessPartner extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String BUSINESS_PARTNER_EDL_PROCESS_ID = "035D9EA348174656B83E4CADE2380C46";
  public static final String SHARAF_DUMMY_PRICELIST_FOR_BUSINESS_PARTNERS = "Sharaf_Dummy_Pricelist";
  public static final String TABLE_NAME = "obshint_bpartner";
  public static final String TABLE_COLUMN_ID = "obshint_bpartner_id";
  public static final String CASH_PAYMENT_METHOD = "Cash";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(BUSINESS_PARTNER_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateBusinessPartner(String item) {
    String msg = "";
    BusinessPartner stagingBusinessPartner = OBDal.getInstance().get(BusinessPartner.class, item);
    try {
      if (stagingBusinessPartner != null) {
        boolean save = false;
        Category businessPartnerCategory = getBusinessPartnerCategoryBySeachKey(stagingBusinessPartner
            .getBpartnerCategory());
        if (businessPartnerCategory != null) {
          PriceList priceList = getPriceListByName(SHARAF_DUMMY_PRICELIST_FOR_BUSINESS_PARTNERS);
          if (priceList != null) {
            org.openbravo.model.common.businesspartner.BusinessPartner businessPartner = getBusinessPartnerBySearchKey(
                stagingBusinessPartner.getBpartnerCode(), stagingBusinessPartner.getOrganization());
            if (businessPartner == null) {
              businessPartner = OBProvider.getInstance().get(
                  org.openbravo.model.common.businesspartner.BusinessPartner.class);
              businessPartner.setId(stagingBusinessPartner.getId());
              businessPartner.setNewOBObject(true);
              save = true;
            }
            businessPartner.setClient(stagingBusinessPartner.getClient());
            businessPartner.setOrganization(stagingBusinessPartner.getOrganization());
            businessPartner.setActive(stagingBusinessPartner.isActive());
            businessPartner.setName(stagingBusinessPartner.getBpartnerName());
            businessPartner.setSearchKey(stagingBusinessPartner.getBpartnerCode());
            businessPartner.setBusinessPartnerCategory(businessPartnerCategory);
            businessPartner.setCustomer(stagingBusinessPartner.isCustomer());
            businessPartner.setCreditLimit(stagingBusinessPartner.getCreditLimit());
            businessPartner.setCreditUsed(stagingBusinessPartner.getCreditused());
            businessPartner.setCustshaDgpstatus(stagingBusinessPartner.getDgpstatus());
            businessPartner.setCustshaDgppoints(stagingBusinessPartner.getDgppoints());
            businessPartner.setCustshaTotalspent(stagingBusinessPartner.getTotalspent());
            businessPartner.setCustshaDgpsetdate(stagingBusinessPartner.getDgpsetdate());
            businessPartner.setCustshaDgpexpirydate(stagingBusinessPartner.getDgpexpirydate());
            businessPartner.setGcnvUniquecreditnote(false);
            if (businessPartner.getPaymentTerms() == null) {
              businessPartner.setPaymentTerms(getDefaultPaymentTerm());
            }
            if (businessPartner.getPaymentMethod() == null) {
              businessPartner.setPaymentMethod(getDefaultCashPaymentMethod());
            }
            if (businessPartner.getPriceList() == null) {
              businessPartner.setPriceList(priceList);
            }

            if (save) {
              OBDal.getInstance().save(businessPartner);
            }
            // User Contact
            String contactName = stagingBusinessPartner.getFirstName() + ""
                + stagingBusinessPartner.getLastName() + stagingBusinessPartner.getMobilePhone();
            if (StringUtils.isNotEmpty(contactName)) {
              contactName = stagingBusinessPartner.getFirstName() + " "
                  + stagingBusinessPartner.getLastName();
              if (StringUtils.isEmpty(contactName.replace(" ", ""))) {
                contactName = stagingBusinessPartner.getMobilePhone();
              }
              save = false;
              User user = getBusinessPartnerContact(businessPartner);
              if (user == null) {
                user = OBProvider.getInstance().get(User.class);
                user.setId(stagingBusinessPartner.getId());
                user.setNewOBObject(true);
                save = true;
              }
              user.setClient(stagingBusinessPartner.getClient());
              user.setOrganization(stagingBusinessPartner.getOrganization());
              user.setActive(stagingBusinessPartner.isActive());
              user.setName(StringUtils.substring(contactName, 0, 60));
              user.setFirstName(stagingBusinessPartner.getFirstName());
              user.setLastName(stagingBusinessPartner.getLastName());
              user.setPhone(stagingBusinessPartner.getMobilePhone());
              user.setEmail(stagingBusinessPartner.getEmail());
              user.setBusinessPartner(businessPartner);
              if (save) {
                OBDal.getInstance().save(user);
              }
              // Business Partner Address
              Country country = getCountryByCode(stagingBusinessPartner.getCountry());
              if (country != null) {
                org.openbravo.model.common.geography.Location address = null;
                save = false;
                Location businessPartnerAddress = getBusinessPartnerAddress(businessPartner);
                if (businessPartnerAddress != null) {
                  address = businessPartnerAddress.getLocationAddress();
                } else {
                  address = OBProvider.getInstance().get(
                      org.openbravo.model.common.geography.Location.class);
                  address.setId(stagingBusinessPartner.getId());
                  address.setNewOBObject(true);
                  businessPartnerAddress = OBProvider.getInstance().get(Location.class);
                  businessPartnerAddress.setId(stagingBusinessPartner.getId());
                  businessPartnerAddress.setNewOBObject(true);
                  save = true;
                }
                address.setClient(stagingBusinessPartner.getClient());
                address.setOrganization(stagingBusinessPartner.getOrganization());
                address.setActive(stagingBusinessPartner.isActive());
                address.setCountry(country);
                address.setCityName(stagingBusinessPartner.getCityName());
                address
                    .setAddressLine1(stagingBusinessPartner.getAddressLine1() != null ? stagingBusinessPartner
                        .getAddressLine1() : country.getName());
                address.setPostalCode(stagingBusinessPartner.getPostalCode());
                if (save) {
                  OBDal.getInstance().save(address);
                }
                businessPartnerAddress.setClient(stagingBusinessPartner.getClient());
                businessPartnerAddress.setOrganization(stagingBusinessPartner.getOrganization());
                businessPartnerAddress.setActive(stagingBusinessPartner.isActive());
                businessPartnerAddress.setInvoiceToAddress(stagingBusinessPartner
                    .isInvoicingAddress());
                businessPartnerAddress.setShipToAddress(stagingBusinessPartner.isShippingAddress());
                businessPartnerAddress.setBusinessPartner(businessPartner);
                businessPartnerAddress.setLocationAddress(address);
                if (save) {
                  OBDal.getInstance().save(businessPartnerAddress);
                }
                stagingBusinessPartner.setProcessed(true);
                stagingBusinessPartner.setProcessRun(null);
              } else {
                msg = String.format(OBMessageUtils.messageBD("OBSHINT_COUNTRY_NOT_FOUND"),
                    stagingBusinessPartner.getCountry());
                throw new OBException(msg);
              }
            } else {
              msg = OBMessageUtils.messageBD("OBSHINT_CONTACT_NAME_EMPTY");
              throw new OBException(msg);
            }
          } else {
            msg = String.format(OBMessageUtils.messageBD("OBSHINT_DUMMY_BP_PRICELIST_NOT_FOUND"));
            throw new OBException(msg);
          }
        } else {
          msg = String.format(
              OBMessageUtils.messageBD("OBSHINT_BUSINESS_PARTNER_CATEGORY_NOT_FOUND"),
              stagingBusinessPartner.getBpartnerCategory());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static org.openbravo.model.common.businesspartner.BusinessPartner getBusinessPartnerBySearchKey(
      String businessPartnerSearchKey, Organization organization) {
    org.openbravo.model.common.businesspartner.BusinessPartner businessPartner = null;
    if (StringUtils.isNotEmpty(businessPartnerSearchKey)) {
      OBCriteria<org.openbravo.model.common.businesspartner.BusinessPartner> obCriteria = OBDal
          .getInstance().createCriteria(
              org.openbravo.model.common.businesspartner.BusinessPartner.class);
      obCriteria.add(Restrictions.eq(
          org.openbravo.model.common.businesspartner.BusinessPartner.PROPERTY_SEARCHKEY,
          businessPartnerSearchKey));
      obCriteria.add(Restrictions.eq(
          org.openbravo.model.common.businesspartner.BusinessPartner.PROPERTY_ORGANIZATION,
          organization));
      obCriteria.setFilterOnActive(false);
      businessPartner = (org.openbravo.model.common.businesspartner.BusinessPartner) obCriteria
          .uniqueResult();
    }
    return businessPartner;
  }

  public static User getBusinessPartnerContact(
      org.openbravo.model.common.businesspartner.BusinessPartner businessPartner) {
    User user = null;
    if (businessPartner != null) {
      OBCriteria<User> obCriteria = OBDal.getInstance().createCriteria(User.class);
      obCriteria.add(Restrictions.eq(User.PROPERTY_BUSINESSPARTNER, businessPartner));
      obCriteria.setFilterOnActive(false);
      List<User> users = obCriteria.list();
      if (!users.isEmpty()) {
        user = users.get(0);
      }
    }
    return user;
  }

  public static User getUserByMobilePhone(String mobilePhone) {
    User user = null;
    if (StringUtils.isNotEmpty(mobilePhone)) {
      OBCriteria<User> obCriteria = OBDal.getInstance().createCriteria(User.class);
      obCriteria.add(Restrictions.eq(User.PROPERTY_PHONE, mobilePhone));
      obCriteria.setFilterOnActive(false);
      user = (User) obCriteria.uniqueResult();
    }
    return user;
  }

  public static Category getBusinessPartnerCategoryBySeachKey(
      String businessPartnerCategorySearchKey) {
    Category businessPartnerCategory = null;
    if (StringUtils.isNotEmpty(businessPartnerCategorySearchKey)) {
      OBCriteria<Category> obCriteria = OBDal.getInstance().createCriteria(Category.class);
      obCriteria
          .add(Restrictions.eq(Category.PROPERTY_SEARCHKEY, businessPartnerCategorySearchKey));
      obCriteria.setFilterOnActive(false);
      businessPartnerCategory = (Category) obCriteria.uniqueResult();
    }
    return businessPartnerCategory;
  }

  public static Country getCountryByCode(String countryCode) {
    Country country = null;
    if (StringUtils.isNotEmpty(countryCode)) {
      OBCriteria<Country> obCriteria = OBDal.getInstance().createCriteria(Country.class);
      obCriteria.add(Restrictions.eq(Country.PROPERTY_ISOCOUNTRYCODE, countryCode));
      obCriteria.setFilterOnActive(false);
      country = (Country) obCriteria.uniqueResult();
    }
    return country;
  }

  public static Location getBusinessPartnerAddress(
      org.openbravo.model.common.businesspartner.BusinessPartner businessPartner) {
    Location businessPartnerAddress = null;
    if (businessPartner != null && !businessPartner.getBusinessPartnerLocationList().isEmpty()) {
      businessPartnerAddress = businessPartner.getBusinessPartnerLocationList().get(0);
    }
    return businessPartnerAddress;
  }

  public static PriceList getPriceListByName(String name) {
    PriceList priceList = null;
    if (StringUtils.isNotEmpty(name)) {
      OBCriteria<PriceList> obCriteria = OBDal.getInstance().createCriteria(PriceList.class);
      obCriteria.add(Restrictions.eq(PriceList.PROPERTY_NAME, name));
      obCriteria.setFilterOnActive(false);
      priceList = (PriceList) obCriteria.uniqueResult();
    }
    return priceList;
  }

  public static PaymentTerm getDefaultPaymentTerm() {
    PaymentTerm paymentTerm = null;
    OBCriteria<PaymentTerm> obCriteria = OBDal.getInstance().createCriteria(PaymentTerm.class);
    obCriteria.add(Restrictions.eq(PaymentTerm.PROPERTY_ORGANIZATION,
        OBDal.getInstance().get(Organization.class, "0")));
    obCriteria.add(Restrictions.eq(PaymentTerm.PROPERTY_DEFAULT, true));
    obCriteria.setMaxResults(1);
    List<PaymentTerm> paymentTermList = obCriteria.list();
    if (!paymentTermList.isEmpty()) {
      paymentTerm = paymentTermList.get(0);
    }
    return paymentTerm;
  }

  public static FIN_PaymentMethod getDefaultCashPaymentMethod() {
    FIN_PaymentMethod paymentMethod = null;
    OBCriteria<FIN_PaymentMethod> obCriteria = OBDal.getInstance().createCriteria(
        FIN_PaymentMethod.class);
    obCriteria.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_ORGANIZATION, OBDal.getInstance()
        .get(Organization.class, "0")));
    obCriteria.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_NAME, CASH_PAYMENT_METHOD));
    obCriteria.setMaxResults(1);
    List<FIN_PaymentMethod> paymentMethodList = obCriteria.list();
    if (!paymentMethodList.isEmpty()) {
      paymentMethod = paymentMethodList.get(0);
    }
    return paymentMethod;
  }

}