/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.config.OBRETCOProductList;
import org.openbravo.retail.config.OBRETCOProlProduct;
import org.openbravo.retail.posterminal.POSUtils;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.Assortment;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportProductAssortment extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_ASSORTMENT_EDL_PROCESS_ID = "1BD3415307C843E18CD0A3B55E47521F";
  public static final String TABLE_NAME = "obshint_assortment";
  public static final String TABLE_COLUMN_ID = "obshint_assortment_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_ASSORTMENT_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateAssortment(String item) {
    String msg = "";
    Assortment stagingAssortment = OBDal.getInstance().get(Assortment.class, item);
    try {
      if (stagingAssortment != null) {
        boolean save = false;
        Product product = ImportProduct.getProductBySearchKey(stagingAssortment.getSKU());
        if (product != null) {
          OBRETCOProductList assortment = getAssortmentByName(stagingAssortment.getAssortmentName());
          if (assortment != null) {
            OBRETCOProlProduct productAssortment = getProductAssortment(assortment, product);
            if (productAssortment == null) {
              productAssortment = OBProvider.getInstance().get(OBRETCOProlProduct.class);
              productAssortment.setId(stagingAssortment.getId());
              productAssortment.setNewOBObject(true);
              save = true;
            }
            productAssortment.setClient(stagingAssortment.getClient());
            productAssortment.setOrganization(stagingAssortment.getOrganization());
            productAssortment.setActive(stagingAssortment.isActive());
            productAssortment.setObretcoProductlist(assortment);
            productAssortment.setProduct(product);
            if (save) {
              OBDal.getInstance().save(productAssortment);
            }
            stagingAssortment.setProcessed(true);
            stagingAssortment.setProcessRun(null);
          } else {
            msg = String.format(OBMessageUtils.messageBD("OBSHINT_ASSORTMENT_NOT_FOUND"),
                stagingAssortment.getAssortmentName());
            throw new OBException(msg);
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
              stagingAssortment.getSKU());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static OBRETCOProductList getAssortmentByOrganization(Organization organization) {
    OBRETCOProductList assortment = null;
    if (organization != null) {
      assortment = POSUtils.getProductListByOrgId(organization.getId());
    }
    return assortment;
  }

  public static OBRETCOProductList getAssortmentByName(String assortmentName) {
    OBRETCOProductList assortment = null;
    if (StringUtils.isNotEmpty(assortmentName)) {
      OBCriteria<OBRETCOProductList> obCriteria = OBDal.getInstance().createCriteria(
          OBRETCOProductList.class);
      obCriteria.add(Restrictions.eq(OBRETCOProductList.PROPERTY_NAME, assortmentName));
      obCriteria.setFilterOnActive(false);
      List<OBRETCOProductList> assortments = obCriteria.list();
      if (!assortments.isEmpty()) {
        assortment = assortments.get(0);
      }
    }
    return assortment;
  }

  public static OBRETCOProlProduct getProductAssortment(OBRETCOProductList assortment,
      Product product) {
    OBRETCOProlProduct productAssortment = null;
    if (assortment != null && product != null) {
      OBCriteria<OBRETCOProlProduct> obCriteria = OBDal.getInstance().createCriteria(
          OBRETCOProlProduct.class);
      obCriteria.add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_OBRETCOPRODUCTLIST, assortment));
      obCriteria.add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_PRODUCT, product));
      obCriteria.setFilterOnActive(false);
      productAssortment = (OBRETCOProlProduct) obCriteria.uniqueResult();
    }
    return productAssortment;
  }

}