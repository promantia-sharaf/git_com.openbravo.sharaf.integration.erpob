/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.ProductCategory;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportProductCategory extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_CATEGORY_EDL_PROCESS_ID = "9047A0E9FD9143F793C6913C71326963";
  public static final String TABLE_NAME = "obshint_product_category";
  public static final String TABLE_COLUMN_ID = "obshint_product_category_id";
  public static final String M_PRODUCT_CATEGORY_TABLE_ID = "209";
  public static final String M_PRODUCT_CATEGORY_TREE_TYPE_AREA = "PC";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_CATEGORY_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProductCategory(String item) {
    String msg = "";
    ProductCategory stagingProductCategory = OBDal.getInstance().get(ProductCategory.class, item);
    try {
      if (stagingProductCategory != null) {
        if (stagingProductCategory.isActive().equals(false)) {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_CATEGORY_NOT_ACTIVE"),
              stagingProductCategory.getCategoryCode());
          throw new OBException(msg);
        }
        boolean save = false;
        org.openbravo.model.common.plm.ProductCategory productCategory = getProductCategoryBySearchKey(stagingProductCategory
            .getCategoryCode());
        if (productCategory == null) {
          productCategory = OBProvider.getInstance().get(
              org.openbravo.model.common.plm.ProductCategory.class);
          productCategory.setId(stagingProductCategory.getId());
          productCategory.setNewOBObject(true);
          save = true;
        }
        productCategory.setClient(stagingProductCategory.getClient());
        productCategory.setOrganization(stagingProductCategory.getOrganization());
        productCategory.setActive(stagingProductCategory.isActive());
        productCategory
            .setName(StringUtils.isNotEmpty(stagingProductCategory.getCategoryName()) ? StringUtils
                .substring(stagingProductCategory.getCategoryName(), 0, 60)
                : stagingProductCategory.getCategoryCode());
        productCategory.setSearchKey(stagingProductCategory.getCategoryCode());
        productCategory.setCustshaCategoryType(stagingProductCategory.getCategoryType());
        productCategory.setPlannedMargin(new BigDecimal(0));
        if (save) {
          OBDal.getInstance().save(productCategory);
        }
        if (StringUtils.isNotEmpty(stagingProductCategory.getParentCategoryCode())) {
          org.openbravo.model.common.plm.ProductCategory parentProductCategory = getProductCategoryBySearchKey(stagingProductCategory
              .getParentCategoryCode());
          if (parentProductCategory != null) {
            Tree sharafProductCategoryTree = getSharafProductCategoryTree();
            if (sharafProductCategoryTree != null) {
              OBDal.getInstance().flush();
              save = false;
              TreeNode productCategoryNode = getProductCategoryNode(sharafProductCategoryTree,
                  productCategory);
              if (productCategoryNode == null) {
                productCategoryNode = OBProvider.getInstance().get(TreeNode.class);
                productCategoryNode.setId(stagingProductCategory.getId());
                productCategoryNode.setNewOBObject(true);
                save = true;
              }
              productCategoryNode.setClient(stagingProductCategory.getClient());
              productCategoryNode.setOrganization(stagingProductCategory.getOrganization());
              productCategoryNode.setActive(stagingProductCategory.isActive());
              productCategoryNode.setTree(sharafProductCategoryTree);
              productCategoryNode.setNode(productCategory.getId());
              productCategoryNode.setReportSet(parentProductCategory.getId());
              productCategoryNode.setSequenceNumber(new Long(999));
              if (save) {
                OBDal.getInstance().save(productCategoryNode);
              }
              if (!parentProductCategory.isSummaryLevel()) {
                parentProductCategory.setSummaryLevel(true);
                OBDal.getInstance().save(parentProductCategory);
              }
            } else {
              msg = OBMessageUtils.messageBD("OBSHINT_PRODUCT_CATEGORY_TREE_NOT_FOUND");
              throw new OBException(msg);
            }
          } else {
            msg = String.format(
                OBMessageUtils.messageBD("OBSHINT_PARENT_PRODUCT_CATEGORY_NOT_FOUND"),
                stagingProductCategory.getParentCategoryCode());
            throw new OBException(msg);
          }
        }
        stagingProductCategory.setProcessed(true);
        stagingProductCategory.setProcessRun(null);
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static org.openbravo.model.common.plm.ProductCategory getProductCategoryBySearchKey(
      String productCategorySearchKey) {
    org.openbravo.model.common.plm.ProductCategory productCategory = null;
    if (StringUtils.isNotEmpty(productCategorySearchKey)) {
      OBCriteria<org.openbravo.model.common.plm.ProductCategory> obCriteria = OBDal.getInstance()
          .createCriteria(org.openbravo.model.common.plm.ProductCategory.class);
      obCriteria.add(Restrictions.eq(
          org.openbravo.model.common.plm.ProductCategory.PROPERTY_SEARCHKEY,
          productCategorySearchKey));
      obCriteria.setFilterOnActive(false);
      productCategory = (org.openbravo.model.common.plm.ProductCategory) obCriteria.uniqueResult();
    }
    return productCategory;
  }

  public static TreeNode getProductCategoryNode(Tree productCategoryTree,
      org.openbravo.model.common.plm.ProductCategory productCategory) {
    TreeNode productCategoryNode = null;
    if (productCategory != null) {
      OBCriteria<TreeNode> obCriteria = OBDal.getInstance().createCriteria(TreeNode.class);
      obCriteria.add(Restrictions.eq(TreeNode.PROPERTY_NODE, productCategory.getId()));
      obCriteria.add(Restrictions.eq(TreeNode.PROPERTY_TREE, productCategoryTree));
      obCriteria.setFilterOnActive(false);
      productCategoryNode = (TreeNode) obCriteria.uniqueResult();
    }
    return productCategoryNode;
  }

  public static Tree getSharafProductCategoryTree() {
    Tree sharafProductCategoryTree = null;
    OBCriteria<Tree> treeObc = OBDal.getInstance().createCriteria(Tree.class);
    treeObc.add(Restrictions.eq(Tree.PROPERTY_TABLE,
        OBDal.getInstance().get(Table.class, M_PRODUCT_CATEGORY_TABLE_ID)));
    treeObc.add(Restrictions.eq(Tree.PROPERTY_TYPEAREA, M_PRODUCT_CATEGORY_TREE_TYPE_AREA));
    sharafProductCategoryTree = (Tree) treeObc.uniqueResult();
    return sharafProductCategoryTree;
  }

}