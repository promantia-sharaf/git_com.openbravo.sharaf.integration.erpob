/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.Product;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;
import com.openbravo.sharaf.integration.erpob.utility.AlertUtils;

public class ImportProduct extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_EDL_PROCESS_ID = "448DF336D1064BDB87DB71EA0137CF10";
  public static final String TABLE_NAME = "obshint_product";
  public static final String TABLE_COLUMN_ID = "obshint_product_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor.addRequest(PRODUCT_EDL_PROCESS_ID, ImportMasterDataItemProcessor
            .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProduct(String item) {
    String msg = "";
    Product stagingProduct = OBDal.getInstance().get(Product.class, item);
    try {
      if (stagingProduct != null) {
        boolean save = false;
        UOM uom = getUOMByName(stagingProduct.getUnitOfMeasure());
        if (uom != null) {
          ProductCategory productCategory = ImportProductCategory
              .getProductCategoryBySearchKey(stagingProduct.getCategoryCode());
          if (productCategory != null) {
            TaxCategory taxCategory = getTaxCategoryByName(stagingProduct.getTAXCategory());
            if (taxCategory != null) {
              org.openbravo.model.common.plm.Product product = getProductBySearchKey(
                  stagingProduct.getSKU());
              if (product == null) {
                product = OBProvider.getInstance()
                    .get(org.openbravo.model.common.plm.Product.class);
                product.setId(stagingProduct.getId());
                product.setNewOBObject(true);
                save = true;
              }
              product.setClient(stagingProduct.getClient());
              product.setOrganization(stagingProduct.getOrganization());
              product.setActive(stagingProduct.isActive());
              product.setName(StringUtils.isNotEmpty(stagingProduct.getName())
                  ? StringUtils.substring(stagingProduct.getName(), 0, 60)
                  : stagingProduct.getSKU());
              product.setSearchKey(stagingProduct.getSKU());
              product.setDescription(stagingProduct.getName());
              product.setCustshaArabicName(stagingProduct.getArabicName());
              product.setCustqcIsgiftcard(stagingProduct.isGiftcardProduct());
              product.setCustqcIsreloadgiftcard(stagingProduct.isGiftcardReloadProduct());
              product.setCustshaDeptType(stagingProduct.getDepartmentType());
              product.setCustrfdIsRfidEnabled(stagingProduct.isRfidEnabled());
              product.setCustrfdIsSerialnoEnabled(stagingProduct.isSerialNoEnabled());
              String brandName = ImportBrand
                  .getStagingBrandNameByCode(stagingProduct.getBrandCode());
              product.setBrand(ImportBrand.getBrandByName(brandName));
              product.setUOM(uom);
              product.setProductCategory(productCategory);
              product.setProductType(getProductTypeSearchKey(stagingProduct.getProductType()));
              product.setCUSTDELDeliveryCondition(stagingProduct.getDeliveryType());
              product.setStocked(false);
              product.setTaxCategory(taxCategory);
              product.setCustshaModelCode(stagingProduct.getModelCode());
              product.setObposShowstock(false);
              product.setOBPOSAllowAnonymousSale(true);
              product.setObposGroupedproduct(false);
              product.setCustshaIrType(stagingProduct.getInstantRedemptionType());
              product.setCustshaIrAmount(stagingProduct.getInstantRedemptionAmount());
              product.setCustshaDgshieldEnabled(stagingProduct.isDgshieldEnabled());
              product.setCustshaDgspremiumEnabled(stagingProduct.isDgshieldpremiumEnabled());
              product.setCustshaExtdwtyEnabled(stagingProduct.isExtendedwarantyEnabled());
              product.setCustshaDgsplusEnabled(stagingProduct.isDgsplusEnabled());
              product.setCustshaApplecareEnabled(stagingProduct.isApplecareEnabled());
              product.setCustshaEwdpdoctypeEnabled(stagingProduct.isEwdpdoctypeEnabled());//ewdpdoctype_enabled
              if (save) {
                OBDal.getInstance().save(product);
              }
              stagingProduct.setProcessed(true);
              stagingProduct.setProcessRun(null);
            } else {
              msg = String.format(OBMessageUtils.messageBD("OBSHINT_TAX_CATEGORY_NOT_FOUND"),
                  stagingProduct.getTAXCategory());
              throw new OBException(msg);
            }
          } else {
            msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_CATEGORY_NOT_FOUND"),
                stagingProduct.getCategoryCode());
            throw new OBException(msg);
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_UOM_NOT_FOUND"),
              stagingProduct.getUnitOfMeasure());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static org.openbravo.model.common.plm.Product getProductBySearchKey(
      String productSearchKey) {
    org.openbravo.model.common.plm.Product product = null;
    if (StringUtils.isNotEmpty(productSearchKey)) {
      OBCriteria<org.openbravo.model.common.plm.Product> obCriteria = OBDal.getInstance()
          .createCriteria(org.openbravo.model.common.plm.Product.class);
      obCriteria.add(Restrictions.eq(org.openbravo.model.common.plm.Product.PROPERTY_SEARCHKEY,
          productSearchKey));
      obCriteria.setFilterOnActive(false);
      product = (org.openbravo.model.common.plm.Product) obCriteria.uniqueResult();
    }
    return product;
  }

  public static UOM getUOMByName(String uomName) {
    UOM uom = null;
    if (StringUtils.isNotEmpty(uomName)) {
      OBCriteria<UOM> obCriteria = OBDal.getInstance().createCriteria(UOM.class);
      obCriteria.add(Restrictions.eq(UOM.PROPERTY_NAME, uomName));
      obCriteria.setFilterOnActive(false);
      List<UOM> uoms = obCriteria.list();
      if (!uoms.isEmpty()) {
        uom = uoms.get(0);
      }
    }
    return uom;
  }

  public static TaxCategory getTaxCategoryByName(String taxCategoryName) {
    TaxCategory taxCategory = null;
    if (StringUtils.isNotEmpty(taxCategoryName)) {
      OBCriteria<TaxCategory> obCriteria = OBDal.getInstance().createCriteria(TaxCategory.class);
      obCriteria.add(Restrictions.eq(TaxCategory.PROPERTY_NAME, taxCategoryName));
      obCriteria.setFilterOnActive(false);
      List<TaxCategory> taxCategorys = obCriteria.list();
      if (!taxCategorys.isEmpty()) {
        taxCategory = taxCategorys.get(0);
      }
    }
    return taxCategory;
  }

  public static String getProductTypeSearchKey(String productTypeName) {
    String productTypeSearchKey = "I";
    if (StringUtils.isNotEmpty(productTypeName)) {
      OBCriteria<org.openbravo.model.ad.domain.List> obCriteria = OBDal.getInstance()
          .createCriteria(org.openbravo.model.ad.domain.List.class);
      obCriteria
          .add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_NAME, productTypeName));
      obCriteria.add(
          Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE + ".id", "270"));
      obCriteria.setFilterOnActive(false);
      List<org.openbravo.model.ad.domain.List> productTypes = obCriteria.list();
      if (!productTypes.isEmpty()) {
        org.openbravo.model.ad.domain.List productType = productTypes.get(0);
        productTypeSearchKey = productType.getSearchKey();
      }
    }
    return productTypeSearchKey;
  }

  public static void createSendAlert(Product stagingProduct, String msg) {
    if (stagingProduct != null && StringUtils.isNotEmpty(msg)) {
      AlertUtils.createSendAlert(stagingProduct.getClient(), stagingProduct.getOrganization(),
          AlertUtils.PRODUCT_ALERT_RULE_ID, msg, stagingProduct.getIdentifier(),
          stagingProduct.getId());
    }
  }

}
