/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.Brand;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;

public class ImportBrand extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String BRAND_EDL_PROCESS_ID = "2A6C6EA2FE694E95BE3FB2B17B5C2699";
  public static final String TABLE_NAME = "obshint_brand";
  public static final String TABLE_COLUMN_ID = "obshint_brand_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(BRAND_EDL_PROCESS_ID, ImportMasterDataItemProcessor.getRecordsToProcess(
                TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateBrand(String item) {
    Brand stagingBrand = OBDal.getInstance().get(Brand.class, item);
    try {
      if (stagingBrand != null) {
        boolean save = false;
        org.openbravo.model.common.plm.Brand brand = getBrandByName(stagingBrand.getBrandName()
            + " (" + stagingBrand.getBrandCode() + ")");
        if (brand == null) {
          brand = OBProvider.getInstance().get(org.openbravo.model.common.plm.Brand.class);
          brand.setId(stagingBrand.getId());
          brand.setNewOBObject(true);
          save = true;
        }
        brand.setClient(stagingBrand.getClient());
        brand.setOrganization(stagingBrand.getOrganization());
        brand.setActive(stagingBrand.isActive());
        brand.setName(stagingBrand.getBrandName() + " (" + stagingBrand.getBrandCode() + ")");
        brand.setDescription(stagingBrand.getBrandName());
        if (save) {
          OBDal.getInstance().save(brand);
        }
        stagingBrand.setProcessed(true);
        stagingBrand.setProcessRun(null);
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static String getStagingBrandNameByCode(String brandCode) {
    Brand stagingBrand = null;
    if (StringUtils.isNotEmpty(brandCode)) {
      OBCriteria<Brand> obCriteria = OBDal.getInstance().createCriteria(Brand.class);
      obCriteria.add(Restrictions.eq(Brand.PROPERTY_BRANDCODE, brandCode));
      obCriteria.setFilterOnActive(false);
      List<Brand> brands = obCriteria.list();
      if (!brands.isEmpty()) {
        stagingBrand = brands.get(0);
      } else {
        return null;
      }
    }
    return stagingBrand.getBrandName() + " (" + stagingBrand.getBrandCode() + ")";
  }

  public static org.openbravo.model.common.plm.Brand getBrandByName(String brandName) {
    org.openbravo.model.common.plm.Brand brand = null;
    if (StringUtils.isNotEmpty(brandName)) {
      OBCriteria<org.openbravo.model.common.plm.Brand> obCriteria = OBDal.getInstance()
          .createCriteria(org.openbravo.model.common.plm.Brand.class);
      obCriteria
          .add(Restrictions.eq(org.openbravo.model.common.plm.Brand.PROPERTY_NAME, brandName));
      obCriteria.setFilterOnActive(false);
      List<org.openbravo.model.common.plm.Brand> brands = obCriteria.list();
      if (!brands.isEmpty()) {
        brand = brands.get(0);
      }
    }
    return brand;
  }

}