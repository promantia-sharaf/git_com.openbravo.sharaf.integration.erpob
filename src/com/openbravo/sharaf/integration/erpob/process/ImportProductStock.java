/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.process.ProcessRequest;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.quartz.JobExecutionException;

import com.openbravo.sharaf.integration.erpob.ProductStock;
import com.openbravo.sharaf.integration.erpob.edl.ImportMasterDataItemProcessor;
import com.openbravo.sharaf.retail.customdevelopments.OBProductStock;

public class ImportProductStock extends DalBaseProcess {

  private static ProcessLogger logger;

  public static final String PRODUCT_STOCK_EDL_PROCESS_ID = "A12A9B4DC88642538463A31E855B3CD3";
  public static final String TABLE_NAME = "obshint_product_stock";
  public static final String TABLE_COLUMN_ID = "obshint_product_stock_id";
  static OBEDLRequest edlRequest = null;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    ConnectionProvider conn = new DalConnectionProvider(false);
    String clientId = OBContext.getOBContext().getCurrentClient().getId();
    String adProcessRunId = bundle.getProcessRunId();

    try {
      OBContext.setAdminMode(true);
      // update "ad_process_run" column to current ad_process_run of records to be processed
      int recordsToProcess = ImportMasterDataItemProcessor
          .updateRecordsProcessRunToCurrentProcessRun(conn, clientId, TABLE_NAME, TABLE_COLUMN_ID,
              adProcessRunId);
      logger.logln(recordsToProcess + " records to be processed");
      if (recordsToProcess > 0) {
        // Commit
        ImportMasterDataItemProcessor.doCommit();
        ProcessRequest<?, ?> processor = ProcessRequest.getProcessRequestInstance();
        edlRequest = processor
            .addRequest(PRODUCT_STOCK_EDL_PROCESS_ID, ImportMasterDataItemProcessor
                .getRecordsToProcess(TABLE_NAME, clientId, adProcessRunId));
      }
    } catch (Exception e) {
      logger.logln(e.getMessage());
      // rollback "ad_process_run" column to null of records to be processed
      ImportMasterDataItemProcessor.updateRecordsProcessRunToNull(conn, clientId, TABLE_NAME,
          TABLE_COLUMN_ID, adProcessRunId);
      ImportMasterDataItemProcessor.doCommit();
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static void createOrUpdateProductStock(String item) {
    String msg = "";
    ProductStock stagingProductStock = OBDal.getInstance().get(ProductStock.class, item);
    try {
      if (stagingProductStock != null) {
        boolean save = false;
        Product product = ImportProduct.getProductBySearchKey(stagingProductStock.getSKU());
        if (product != null) {
          Warehouse warehouse = getWarehouseBySearchKey(stagingProductStock.getWarehouseCode());
          if (warehouse != null) {
            OBProductStock productStock = getProductStockByProductAndWarehouse(product, warehouse);
            if (productStock == null) {
              productStock = OBProvider.getInstance().get(OBProductStock.class);
              productStock.setId(stagingProductStock.getId());
              productStock.setNewOBObject(true);
              save = true;
            }
            productStock.setClient(stagingProductStock.getClient());
            productStock.setOrganization(stagingProductStock.getOrganization());
            productStock.setActive(stagingProductStock.isActive());
            productStock.setProduct(product);
            productStock.setWarehouse(warehouse);
            productStock.setQuantityOnHand(stagingProductStock.getQuantity());
            if (save) {
              OBDal.getInstance().save(productStock);
            }
            stagingProductStock.setProcessed(true);
            stagingProductStock.setProcessRun(null);
          } else {
            msg = String.format(OBMessageUtils.messageBD("OBSHINT_WAREHOUSE_NOT_FOUND"),
                stagingProductStock.getWarehouseCode());
            throw new OBException(msg);
          }
        } else {
          msg = String.format(OBMessageUtils.messageBD("OBSHINT_PRODUCT_NOT_FOUND"),
              stagingProductStock.getSKU());
          throw new OBException(msg);
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static OBProductStock getProductStockByProductAndWarehouse(Product product,
      Warehouse warehouse) {
    OBProductStock productStock = null;
    if (product != null && warehouse != null) {
      OBCriteria<OBProductStock> obCriteria = OBDal.getInstance().createCriteria(
          OBProductStock.class);
      obCriteria.add(Restrictions.eq(OBProductStock.PROPERTY_PRODUCT, product));
      obCriteria.add(Restrictions.eq(OBProductStock.PROPERTY_WAREHOUSE, warehouse));
      List<OBProductStock> productStocks = obCriteria.list();
      if (!productStocks.isEmpty()) {
        productStock = productStocks.get(0);
      }
    }
    return productStock;
  }

  public static StorageDetail getProductStockByProductAndStorageBin(Product product,
      Locator storageBin) {
    StorageDetail productStock = null;
    if (product != null && storageBin != null) {
      OBCriteria<StorageDetail> obCriteria = OBDal.getInstance()
          .createCriteria(StorageDetail.class);
      obCriteria.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
      obCriteria.add(Restrictions.eq(StorageDetail.PROPERTY_STORAGEBIN, storageBin));
      List<StorageDetail> productStocks = obCriteria.list();
      if (!productStocks.isEmpty()) {
        productStock = productStocks.get(0);
      }
    }
    return productStock;
  }

  public static Warehouse getWarehouseBySearchKey(String warehouseCode) {
    Warehouse warehouse = null;
    if (StringUtils.isNotEmpty(warehouseCode)) {
      OBCriteria<Warehouse> obCriteria = OBDal.getInstance().createCriteria(Warehouse.class);
      obCriteria.add(Restrictions.eq(Warehouse.PROPERTY_SEARCHKEY, warehouseCode));
      List<Warehouse> warehouses = obCriteria.list();
      if (!warehouses.isEmpty()) {
        warehouse = warehouses.get(0);
      }
    }
    return warehouse;
  }

  public static Locator getStorageBinByOrganization(Organization organization) {
    Locator storageBin = null;
    if (organization != null && !organization.getOrganizationWarehouseList().isEmpty()) {
      Warehouse warehouse = organization.getOrganizationWarehouseList().get(0).getWarehouse();
      if (!warehouse.getLocatorList().isEmpty()) {
        storageBin = warehouse.getLocatorList().get(0);
      }
    }
    return storageBin;
  }

}