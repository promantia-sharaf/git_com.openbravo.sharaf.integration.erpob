/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.erpob.event;

import javax.enterprise.event.Observes;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBDal;
import org.openbravo.externaldata.integration.OBEDLRequest;
import org.openbravo.externaldata.integration.OBEDLRequestLine;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;

import com.openbravo.sharaf.integration.erpob.Assortment;
import com.openbravo.sharaf.integration.erpob.BusinessPartner;
import com.openbravo.sharaf.integration.erpob.OfferIn;
import com.openbravo.sharaf.integration.erpob.Product;
import com.openbravo.sharaf.integration.erpob.ProductBarcode;
import com.openbravo.sharaf.integration.erpob.ProductCategory;
import com.openbravo.sharaf.integration.erpob.ProductPrice;
import com.openbravo.sharaf.integration.erpob.ProductStock;
import com.openbravo.sharaf.integration.erpob.ProductTaxCategory;
import com.openbravo.sharaf.integration.erpob.utility.AlertUtils;
import com.openbravo.sharaf.integration.erpob.utility.ERPOBUtils;

public class OBEDLRequestLineEventHandler extends EntityPersistenceEventObserver {

  private static final Entity[] OBSERVED_ENTITIES = { ModelProvider.getInstance().getEntity(
      OBEDLRequestLine.ENTITY_NAME) };
  private static final Entity requestLineEntity = ModelProvider.getInstance().getEntity(
      OBEDLRequestLine.ENTITY_NAME);
  private static final Property status = requestLineEntity
      .getProperty(OBEDLRequestLine.PROPERTY_STATUS);

  @Override
  protected Entity[] getObservedEntities() {
    return OBSERVED_ENTITIES;
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    OBEDLRequestLine requestLine = (OBEDLRequestLine) event.getTargetInstance();
    String edlProcessId = requestLine.getEDLRequest().getEDLProcess().getId();
    if (!ERPOBUtils.isSharafImportEDLProcess(edlProcessId)) {
      return;
    }
    String currentStatus = (String) event.getCurrentState(status);
    if (currentStatus.equals(ERPOBUtils.EDLLine_Status_Error)) {
      String stagingRecordId = requestLine.getLinedata();
      stagingRecordId = StringUtils.substring(stagingRecordId, 0, 32);
      setStagingAsProcessedAndCreateSendAlert(stagingRecordId, edlProcessId, requestLine);
      System.out.println("Save: " + currentStatus);
    }
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
  }

  public static void setStagingAsProcessedAndCreateSendAlert(String id, String edProcessId,
      OBEDLRequestLine requestLine) {
    String msg = requestLine.getErrorMsg();
    OBEDLRequest edlRequest = requestLine.getEDLRequest();
    if (edProcessId.equals(ERPOBUtils.PRODUCT_ASSORTMENT_EDL_PROCESS_ID)) {
      Assortment stagingAssortment = OBDal.getInstance().get(Assortment.class, id);
      stagingAssortment.setObedlRequest(edlRequest);
      stagingAssortment.setProcessRun(null);
      createSendAlert(stagingAssortment.getClient(), stagingAssortment.getOrganization(),
          AlertUtils.PRODUCT_ASSORTMENT_ALERT_RULE_ID, msg, stagingAssortment.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PROMOTION_EDL_PROCESS_ID)) {
      OfferIn stagingPromotion = OBDal.getInstance().get(OfferIn.class, id);
      stagingPromotion.setProcessRun(null);
      stagingPromotion.setObedlRequest(edlRequest);
      createSendAlert(stagingPromotion.getClient(), stagingPromotion.getOrganization(),
          AlertUtils.PROMOTION_ALERT_RULE_ID, msg, stagingPromotion.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.BRAND_EDL_PROCESS_ID)) {
      OfferIn stagingBrand = OBDal.getInstance().get(OfferIn.class, id);
      stagingBrand.setProcessRun(null);
      stagingBrand.setObedlRequest(edlRequest);
      createSendAlert(stagingBrand.getClient(), stagingBrand.getOrganization(),
          AlertUtils.BRAND_ALERT_RULE_ID, msg, stagingBrand.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.BUSINESS_PARTNER_EDL_PROCESS_ID)) {
      BusinessPartner businessPartner = OBDal.getInstance().get(BusinessPartner.class, id);
      businessPartner.setProcessRun(null);
      businessPartner.setObedlRequest(edlRequest);
      createSendAlert(businessPartner.getClient(), businessPartner.getOrganization(),
          AlertUtils.BUSINESS_PARTNER_ALERT_RULE_ID, msg, businessPartner.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_EDL_PROCESS_ID)) {
      Product stagingProduct = OBDal.getInstance().get(Product.class, id);
      stagingProduct.setProcessRun(null);
      stagingProduct.setObedlRequest(edlRequest);
      createSendAlert(stagingProduct.getClient(), stagingProduct.getOrganization(),
          AlertUtils.PRODUCT_ALERT_RULE_ID, msg, stagingProduct.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_BARCODE_EDL_PROCESS_ID)) {
      ProductBarcode stagingProductBarcode = OBDal.getInstance().get(ProductBarcode.class, id);
      stagingProductBarcode.setProcessRun(null);
      stagingProductBarcode.setObedlRequest(edlRequest);
      createSendAlert(stagingProductBarcode.getClient(), stagingProductBarcode.getOrganization(),
          AlertUtils.PRODUCT_BARCODE_ALERT_RULE_ID, msg, stagingProductBarcode.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_CATEGORY_EDL_PROCESS_ID)) {
      ProductCategory stagingProductCategory = OBDal.getInstance().get(ProductCategory.class, id);
      stagingProductCategory.setProcessRun(null);
      stagingProductCategory.setObedlRequest(edlRequest);
      createSendAlert(stagingProductCategory.getClient(), stagingProductCategory.getOrganization(),
          AlertUtils.PRODUCT_CATEGORY_ALERT_RULE_ID, msg, stagingProductCategory.getIdentifier(),
          id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_PRICE_EDL_PROCESS_ID)) {
      ProductPrice stagingProductPrice = OBDal.getInstance().get(ProductPrice.class, id);
      stagingProductPrice.setProcessRun(null);
      stagingProductPrice.setObedlRequest(edlRequest);
      createSendAlert(stagingProductPrice.getClient(), stagingProductPrice.getOrganization(),
          AlertUtils.PRODUCT_PRICE_ALERT_RULE_ID, msg, stagingProductPrice.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_STOCK_EDL_PROCESS_ID)) {
      ProductStock stagingProductStock = OBDal.getInstance().get(ProductStock.class, id);
      stagingProductStock.setProcessRun(null);
      stagingProductStock.setObedlRequest(edlRequest);
      createSendAlert(stagingProductStock.getClient(), stagingProductStock.getOrganization(),
          AlertUtils.PRODUCT_STOCK_ALERT_RULE_ID, msg, stagingProductStock.getIdentifier(), id);
    } else if (edProcessId.equals(ERPOBUtils.PRODUCT_TAXCATEGORY_EDL_PROCESS_ID)) {
      ProductTaxCategory stagingProductTaxCategory = OBDal.getInstance().get(
          ProductTaxCategory.class, id);
      stagingProductTaxCategory.setProcessRun(null);
      stagingProductTaxCategory.setObedlRequest(edlRequest);
      createSendAlert(stagingProductTaxCategory.getClient(),
          stagingProductTaxCategory.getOrganization(),
          AlertUtils.PRODUCT_TAXCATEGORY_ALERT_RULE_ID, msg,
          stagingProductTaxCategory.getIdentifier(), id);
    }
  }

  public static void createSendAlert(Client client, Organization org, String alertRuleId,
      String msg, String identifier, String id) {
    if (id != null && StringUtils.isNotEmpty(msg)) {
      AlertUtils.createSendAlert(client, org, alertRuleId, msg, identifier, id);
    }
  }
}