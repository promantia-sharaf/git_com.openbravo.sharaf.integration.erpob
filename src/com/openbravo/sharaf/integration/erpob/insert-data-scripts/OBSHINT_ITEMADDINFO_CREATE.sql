-- Function: public.obshint_itemaddinfo_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_itemaddinfo_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_itemaddinfo_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_itemadditionalinfo
  (obshint_itemadditionalinfo_id, obshint_invoice_id, invoice_reference, item_code, item_serial_number, item_quantity, product_serial, product_rfid, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, i, i, i, i, get_uuid(), get_uuid(), client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;