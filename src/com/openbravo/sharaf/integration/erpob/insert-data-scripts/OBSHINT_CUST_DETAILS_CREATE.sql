-- Function: public.obshint_cust_details_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_cust_details_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_cust_details_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_customer_details
  (obshint_customer_details_id, obshint_invoice_id, invoice_reference, customer_code, customer_mobile, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, i, 'WALKIN', '667667', client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;