-- Function: public.obshint_product_cat_create(numeric, character varying, character varying)

-- DROP FUNCTION public.obshint_product_cat_create(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_product_cat_create(
    records numeric,
    client_id character varying,
    org_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_product_category 
  (obshint_product_category_id, category_code, category_name, category_type, ad_client_id, ad_org_id, client, organization)
  VALUES
  (get_uuid(), 'Prod Cat '||i, 'Prod Cat '||i, 'Group', client_id, org_id, client_id, org_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
