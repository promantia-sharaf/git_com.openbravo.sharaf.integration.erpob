-- Function: public.obshint_promo_line_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_promo_line_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_promo_line_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_offerline_in
  (obshint_offerline_in_id, obshint_offer_in_id, ad_client_id, ad_org_id, createdby, updatedby, promo, erp_promo, secondary_sku, secondary_sku_amount, secondary_sku_price, quantity, client, organization, primary_link_id)
  VALUES
  (i, i, client_id, org_id, user_id, user_id, 'Promo '||i, 'ERP Promo '||i, 'sku2 '||i, i, i, i, client_id, org_id, 'Promo Link '||i);
 
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;