-- Function: public.obshint_delinline_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_delinline_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_delinline_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_delinstruction_line
  (obshint_delinstruction_line_id, obshint_delinstruction_id, deliv_instr_reference, deliv_item_code, deliv_serial_number, deliv_quantity, deliv_packing_instruction, deliv_item_serial_number, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, i, i, i, i, get_uuid(), i, client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;