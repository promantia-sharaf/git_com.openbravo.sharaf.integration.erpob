-- Function: public.obshint_sales_post_ex_create(numeric, character varying, character varying)

-- DROP FUNCTION public.obshint_sales_post_ex_create(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_sales_post_ex_create(
    records numeric,
    client_id character varying,
    org_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_sales_post_except
  (invoice_reference, ad_client_id, ad_org_id, client, organization)
  VALUES
  ('KS1CAS01/00000' || i || '23' || i, client_id, org_id, client_id, org_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;