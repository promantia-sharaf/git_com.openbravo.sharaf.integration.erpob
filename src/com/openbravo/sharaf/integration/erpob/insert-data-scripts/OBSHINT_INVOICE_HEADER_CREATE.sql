-- Function: public.obshint_invoice_header_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_invoice_header_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_invoice_header_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_invoice 
  (obshint_invoice_id, invoice_reference, invoice_date, transaction_type, billing_store_code, trans_start_date_time, trans_end_date_time, till_code, cashier_code, customer_code, currency_code, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, now()-(i*0.002), 'SI', org_id, now()-(i*0.002), now()-(i*0.001), get_uuid(), get_uuid(), get_uuid(), 'AED', client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;