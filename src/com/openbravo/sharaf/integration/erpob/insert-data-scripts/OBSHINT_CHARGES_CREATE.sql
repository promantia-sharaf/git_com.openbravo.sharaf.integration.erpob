-- Function: public.obshint_charges_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_charges_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_charges_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_charges 
  (obshint_charges_id, obshint_invoice_id, invoice_reference, item_code, charge_code, salesman_code, charge_quantity, charge_selling_price, charge_gross_amount, charge_net_amount, charges_serial_number, scanned_serial_number, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, i, i, 'Prod '||i, '100', 1, i, i, i, i, i, client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;