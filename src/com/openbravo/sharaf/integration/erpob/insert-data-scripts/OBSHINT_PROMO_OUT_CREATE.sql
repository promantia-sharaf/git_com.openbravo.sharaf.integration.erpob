-- Function: public.obshint_promo_out_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_promo_out_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_promo_out_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_offer_out
  (obshint_offer_out_id, ad_client_id, ad_org_id, createdby, updatedby, invoice_ref, item_code, scanned_item_no, promo, promo_primary_item, promo_secondary_item, promo_item_flag, loyalty_card_no, loyalty_points, client, organization, reprocess_record, obedl_request_id, obshint_invoice_id)
  VALUES
  (i, client_id, org_id, user_id, user_id, 'InvRef '||i, 'ItemCode '||i, 10, i, 10, 20, 'P'::bpchar, '1234567890123', i, client_id, org_id, 'N', null, i);
 
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;