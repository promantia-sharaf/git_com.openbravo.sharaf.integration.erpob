-- Function: public.obshint_bpartner_create(numeric, character varying, character varying)

-- DROP FUNCTION public.obshint_bpartner_create(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_bpartner_create(
    records numeric,
    client_id character varying,
    org_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_bpartner
  (obshint_bpartner_id, bpartner_code, bpartner_name, first_name, last_name, mobile_phone, bpartner_category, address1, country, dgpstatus, dgppoints, totalspent, dgpsetdate, dgpexpirydate, ad_client_id, ad_org_id, client, organization)
  VALUES
  (get_uuid(), 'BP '||i, 'BP '||i, 'John '||i, 'Doe '||i, '667667', 'Customer', 'Street '||i, 'AE', 'Silver', records-i, i, now()-100, now()+100, client_id, org_id, client_id, org_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
