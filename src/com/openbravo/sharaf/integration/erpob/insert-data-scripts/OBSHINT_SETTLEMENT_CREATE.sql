-- Function: public.obshint_settlement_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_settlement_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_settlement_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_settlement
  (obshint_settlement_id, obshint_invoice_id, invoice_reference, settlement_code, currency_code, settlement_amt_in_local_curr, settlement_quantity, settlement_serial_number, ad_client_id, ad_org_id, client, organization, createdby, updatedby)
  VALUES
  (i, i, i, i, 'AED', i, i, i, client_id, org_id, client_id, org_id, user_id, user_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;