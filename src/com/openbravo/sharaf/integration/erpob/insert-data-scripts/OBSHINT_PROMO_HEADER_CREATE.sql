-- Function: public.obshint_promo_header_create(numeric, character varying, character varying, character varying)

-- DROP FUNCTION public.obshint_promo_header_create(numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_promo_header_create(
    records numeric,
    client_id character varying,
    org_id character varying,
    user_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_offer_in
  (obshint_offer_in_id, ad_client_id, ad_org_id, createdby, updatedby, promo, erp_promo, promo_docno, promo_desc, primary_sku, primary_sku_amount, promo_start, promo_end, total_amount, client, organization, reprocess_record, obedl_request_id, primary_link_id)
  VALUES
  (i, client_id, org_id, user_id, user_id, 'Promo '||i, 'ERP Promo '||i, i, 'Promo desc '||i, 'sku '||i, i, now()-(i*0.002), now()-(i*0.001), i, client_id, org_id, 'N', null, 'Promo Link '||i);
 
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;