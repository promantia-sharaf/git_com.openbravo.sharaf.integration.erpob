-- Function: public.obshint_brand_create(numeric, character varying, character varying)

-- DROP FUNCTION public.obshint_brand_create(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION public.obshint_brand_create(
    records numeric,
    client_id character varying,
    org_id character varying)
  RETURNS void AS
$BODY$ DECLARE 

v_ResultStr VARCHAR(2000):=''; --OBTG:VARCHAR2--

BEGIN

FOR i IN 1..records
LOOP
  INSERT INTO obshint_brand 
  (obshint_brand_id, brand_code, brand_name, ad_client_id, ad_org_id, client, organization)
  VALUES
  (get_uuid(), 'Brand '||i, 'Brand '||i, client_id, org_id, client_id, org_id);
END LOOP;

END ; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
